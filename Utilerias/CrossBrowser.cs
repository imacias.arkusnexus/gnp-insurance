﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using Enum;

namespace Utilerias
{
    public class CrossBrowser
    {
        private IWebDriver _driver;
        private GenerarEvidencias generarEvidencias;

        public CrossBrowser(IWebDriver _driver)
        {
            this._driver = _driver;
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        public IWebDriver Driver(Navegadores.Navegador navegador)
        {
            try
            {
                switch (navegador)
                {
                    case Navegadores.Navegador.Chrome:
                        return new ChromeDriver();

                    case Navegadores.Navegador.Firefox:
                        return new FirefoxDriver();
          
                    default:
                        return default(IWebDriver);
                }
            }

            catch (Exception ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return default(IWebDriver); 
        }
    }
}
