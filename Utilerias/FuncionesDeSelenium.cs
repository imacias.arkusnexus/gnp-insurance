﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using Enum;

namespace Utilerias
{
   public class FuncionesDeSelenium
    {
        private IWebDriver _driver;
        private IWebElement _webElement;
        private SelectElement selectElement;

        private LocatorsEnum locators;
        private GenerarEvidencias generarEvidencias;

        public FuncionesDeSelenium(IWebDriver _driver)
        {
            this._driver = _driver;
        
            locators = new LocatorsEnum();
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        /// Método para buscar locator.
        public IWebElement BuscarElemento(LocatorsEnum.Locators locators, string elemento)
        {
            try
            {
                switch (locators)
                {
                    case LocatorsEnum.Locators.Id:
                        return _driver.FindElement(By.Id(elemento));
                    
                    case LocatorsEnum.Locators.Name:
                        return _driver.FindElement(By.Name(elemento));

                    case LocatorsEnum.Locators.ClassName:
                        return _driver.FindElement(By.ClassName(elemento));

                    case LocatorsEnum.Locators.XPath:
                        return _driver.FindElement(By.XPath(elemento));

                    case LocatorsEnum.Locators.LinkText:
                        return _driver.FindElement(By.LinkText(elemento));

                    case LocatorsEnum.Locators.PartialLinkText:
                        return _driver.FindElement(By.PartialLinkText(elemento));

                    case LocatorsEnum.Locators.CssSelector:
                        return _driver.FindElement(By.CssSelector(elemento));

                    default:
                        return default (IWebElement);
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _webElement;            
        }

        /// Método para seleccionar una opción del drop-down list.
        public void SeleccionarDropDownList(IWebElement locator, string valor)
        {
            try
            {
                selectElement = new SelectElement(locator);
                selectElement.SelectByText(valor);
                // selectElement.SelectByValue(valor);
            }

            catch(NoSuchElementException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }            
        }
    }
}