﻿using NLog;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace Utilerias
{
    public class GenerarEvidencias
    {
        private IWebDriver _driver;
        protected static Logger logger = LogManager.GetCurrentClassLogger();

         /*Constructor*/
        public GenerarEvidencias(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /// <summary>
        /// El usuario verá un texto impreso en consola y el un archivo TXT para tener generarEvidencias. 
        /// </summary>             
        public void ImprimirMensaje(string mensaje)
        {
            Thread.Sleep(3000);
            Console.WriteLine("*** " + mensaje + " ***");
            logger.Info("*** " + mensaje + " ***");
        }

        public String TimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd_HH-mm-ss");
        }

        /// <summary>
        /// Función para capturar pantalla para obtener generarEvidencias de imagenes.
        /// </summary>        
        public void CapturarPantalla()
        {
            try
            {
                String timeStamp = TimeStamp(DateTime.Now);
                
                Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();

                screenshot.SaveAsFile(@"C:\Users\Mind\Desktop\Log\Screenshot\" + timeStamp +  ".png", ScreenshotImageFormat.Png);
                ImprimirMensaje("Pantalla Capturada");
            }

            catch(Exception ex)
            {
                ImprimirMensaje(ex.Message);
            }            
        }
    }
}
