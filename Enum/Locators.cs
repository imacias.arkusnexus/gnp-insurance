﻿namespace Enum
{
   public class LocatorsEnum
    {
        public enum Locators
        {
            Id,
            Name,
            ClassName,
            LinkText,
            PartialLinkText,
            XPath,
            CssSelector
        }
    }
}
