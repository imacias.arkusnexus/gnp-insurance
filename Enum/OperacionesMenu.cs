﻿namespace Enum
{
    public class OperacionesMenu
    {
        public enum Opciones
        {
            Inicio,
            Autoservicio,
            AdministracionDelSistema,
            Comisionables,
            FuncionesDeLaAgencia,
            PagosLiquidaciones,
            ReportesDeLaAgencia
        }
    }
}
