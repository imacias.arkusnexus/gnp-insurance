﻿using OpenQA.Selenium;
using Protractor;
using Utilerias;
using Controllers;

namespace PageObjectModel
{
    public class CambiarClave
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;
        private GenerarEvidencias generarEvidencias;
        private CambiarClaveControllers _cambiarClaveControllers;

        /*Constructor*/
        public CambiarClave(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
            _cambiarClaveControllers = new CambiarClaveControllers(_driver);
        }

        /*Metodos*/
        public void CambiarLaClaveDelSistema(string nuevaClave, string confirmarClave)
        {
            try
            { 
                _cambiarClaveControllers.campoNuevaClave.SendKeys(nuevaClave);
                generarEvidencias.ImprimirMensaje("Nueva Clave: " + nuevaClave.ToString());

                _cambiarClaveControllers.campoConfirmarClave.SendKeys(confirmarClave);
                generarEvidencias.ImprimirMensaje("Confirmar Clave: " + confirmarClave.ToString());
            }


            catch(NoSuchElementException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }
        }
    }
}
