﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class InformacionPersonal
    {
        private IWebDriver _driver;

        private FuncionesDeSelenium funcionesDeSelenium;
        private InformacionPersonalControllers informacionPersonal;
        private GenerarEvidencias evidencias;

        public InformacionPersonal(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        public void LlenarInfoPersonal(InformacionPersonalDto informacionPersonalDto)
        {
            informacionPersonal = new InformacionPersonalControllers(_driver);
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            evidencias = new GenerarEvidencias(_driver);
            
            informacionPersonal.campoNombre.Clear();
            informacionPersonal.campoNombre.SendKeys(informacionPersonalDto.Nombre);
            evidencias.ImprimirMensaje("Nombre: " + informacionPersonalDto.Nombre);

            informacionPersonal.campoApellido.Clear();
            informacionPersonal.campoApellido.SendKeys(informacionPersonalDto.Apellido);
            evidencias.ImprimirMensaje("Apellido: " + informacionPersonalDto.Apellido);

            informacionPersonal.campoCiudad.Clear();
            evidencias.ImprimirMensaje("Ciudad: " + informacionPersonalDto.Ciudad);

            informacionPersonal.campoEstado.Clear();

            funcionesDeSelenium.SeleccionarDropDownList(informacionPersonal.campoIdioma, informacionPersonalDto.Idioma);
            evidencias.ImprimirMensaje("Idioma: " + informacionPersonalDto.Idioma);

            evidencias.CapturarPantalla();

            informacionPersonal.botonDeCerrar.Click();
            evidencias.ImprimirMensaje("Se cerró el módulo de 'Personal Info.'");

         //   return new InformacionPersonalSubMenu(_driver);
        }


    }
}
