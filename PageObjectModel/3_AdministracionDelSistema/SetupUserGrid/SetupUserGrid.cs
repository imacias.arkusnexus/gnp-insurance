﻿using OpenQA.Selenium;
using Utilerias;
using Enum;
using Controllers;
using ControllersConstantes;
using DataTransferObjects;
using System;
using System.Threading;

namespace PageObjectModel
{
    public class SetupUserGrid
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium _funcionesDeSelenium;
        private GenerarEvidencias _generarEvidencias;
        private ConfigurarUsuariosControllers _configurarUsuariosControllers;

        public SetupUserGrid(IWebDriver _driver)
        {
            this._driver = _driver;
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            _generarEvidencias = new GenerarEvidencias(_driver);
            _configurarUsuariosControllers = new ConfigurarUsuariosControllers(_driver);
        }

        public void SelectAnOptionToSearch(SetupUsersDto setupUsersDto)
        {
            try
            {
                _funcionesDeSelenium.SeleccionarDropDownList(_configurarUsuariosControllers.campoDeBusqueda, setupUsersDto.Search);
                _generarEvidencias.ImprimirMensaje("Opción seleccionada: " + setupUsersDto.Search);
            }

            catch(NullReferenceException ex)
            {
                _generarEvidencias.ImprimirMensaje(ex.Message);
            }
        }

        public void TypeASearchValue(SetupUsersDto setupUsersDto)
        {
            if((setupUsersDto.Search == "Ninguno") || (setupUsersDto.Search == "None"))
            {
                _generarEvidencias.ImprimirMensaje("Value: " + setupUsersDto.Search);
            }

            else
            {
                _configurarUsuariosControllers.campoDeValorDeBusqueda.SendKeys(setupUsersDto.SearchValue);
                _generarEvidencias.ImprimirMensaje("Value: " + setupUsersDto.SearchValue);
            }
        }

        public void ClickOnMagnifyingGlassIcon()
        {
            _configurarUsuariosControllers.iconoDeBuscar.Click();
            _generarEvidencias.ImprimirMensaje("Button clicked: " + _configurarUsuariosControllers.iconoDeBuscar.ToString());
        }

        public void SortColumnsAscendingDescending()
        {
            for (int i = 0; i < 2; i++)
            {
                _configurarUsuariosControllers.sortEmail.Click();
                _generarEvidencias.ImprimirMensaje("Sort: " + _configurarUsuariosControllers.sortEmail.Text);
            }

            _generarEvidencias.CapturarPantalla();
        }

        public void EditAnUser()
        {
            _configurarUsuariosControllers.clickOnAnUserToEditIt.Click();
            _generarEvidencias.ImprimirMensaje("User edited.");
        }
    }
}
