﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using ControllersConstantes;
using Utilerias;

namespace PageObjectModel
{
    public class AgregarUsuarioInfoSistema
    {
        private IWebDriver _driver;

        private AgregarUsuarioInformacionSistemaControllers agregarUsuarioInformacionSistemaControllers;
        private FuncionesDeSelenium funcionesDeSelenium;
        private GenerarEvidencias generarEvidencias;

        public AgregarUsuarioInfoSistema(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
            agregarUsuarioInformacionSistemaControllers = new AgregarUsuarioInformacionSistemaControllers(_driver);
        }

        public void ClickEnLaPestanaDeInfoSistema()
        {
            agregarUsuarioInformacionSistemaControllers.systemInfoTab.Click();
            generarEvidencias.ImprimirMensaje("Tab pressed: " + agregarUsuarioInformacionSistemaControllers.systemInfoTab.Text);
        }

        public void LlenarLosCamposDeInfoSistema(AgregarUsuarioInformacionSistemaDto agregarUsuarioInformacionSistema)
        {
            try
            {
                agregarUsuarioInformacionSistemaControllers.campoUsuario.SendKeys(agregarUsuarioInformacionSistema.Usuario);
                generarEvidencias.ImprimirMensaje("User name: " + agregarUsuarioInformacionSistema.Usuario);

                agregarUsuarioInformacionSistemaControllers.campoContrasena.SendKeys(agregarUsuarioInformacionSistema.Contrasena);
                generarEvidencias.ImprimirMensaje("Password: " + agregarUsuarioInformacionSistema.Contrasena);

                agregarUsuarioInformacionSistemaControllers.campoConfirmarContrasena.SendKeys(agregarUsuarioInformacionSistema.ConfirmarContrasena);
                generarEvidencias.ImprimirMensaje("Confirm password: " + agregarUsuarioInformacionSistema.ConfirmarContrasena);

                funcionesDeSelenium.SeleccionarDropDownList(agregarUsuarioInformacionSistemaControllers.campoIdioma, agregarUsuarioInformacionSistema.Idioma);
                generarEvidencias.ImprimirMensaje("Language: " + agregarUsuarioInformacionSistema.Idioma);

                for(int i=0; i<=3; i++)
                {
                    if(i == 0)
                    {
                        agregarUsuarioInformacionSistemaControllers.seleccionarAdministracionDeAgencia.Click();
                        generarEvidencias.ImprimirMensaje("Agency administrator: " + agregarUsuarioInformacionSistemaControllers.seleccionarAdministracionDeAgencia.Text);
                    }

                    if (i == 3)
                    {
                        agregarUsuarioInformacionSistemaControllers.seleccionarAdministracionDeAgencia.Click();
                        generarEvidencias.ImprimirMensaje("Active user: " + agregarUsuarioInformacionSistemaControllers.seleccionarAdministracionDeAgencia.Text);
                    }
                }

                generarEvidencias.CapturarPantalla();
            }

            catch(ElementNotVisibleException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }
            
        }
    }
}
