﻿using OpenQA.Selenium;
using System;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class AgregarUsuariosFuncionesGenerales
    {
        private IWebDriver _driver;
        private GenerarEvidencias generarEvidencias;
        private CommercialTruckFuncionesComunesControllers controllersComunes;

        /*Contructor*/
        public AgregarUsuariosFuncionesGenerales(IWebDriver _driver)
        {
            this._driver = _driver;
            controllersComunes = new CommercialTruckFuncionesComunesControllers(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        /*Métodos*/
        public void ClickEnElBotonDeGrabar()
        {
            try
            {
                controllersComunes.botonDeContinuarDatosPoliza.Click();
                generarEvidencias.ImprimirMensaje("Botón presionado: " + controllersComunes.botonDeContinuarDatosPoliza.Text);
                generarEvidencias.CapturarPantalla();
            }

            catch(NullReferenceException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }
            
        }

        public void ClickEnElBotonDeCerrar()
        {
            try
            {
                controllersComunes.botonDeCerrar.Click();
                generarEvidencias.ImprimirMensaje(controllersComunes.botonDeCerrar.Text);
                generarEvidencias.CapturarPantalla();
            }

            catch (NullReferenceException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }
        }
    }
}
