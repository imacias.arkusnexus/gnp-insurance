﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class AgregarUsuarioInfoPersonal
    {
        private IWebDriver _driver;

        private AgregarUsuarioInformacionPersonalControllers agregarUsuarioInformacionPersonalControllers;

        private FuncionesDeSelenium funcionesDeSelenium;
        private GenerarEvidencias generarEvidencias;
        
        public AgregarUsuarioInfoPersonal(IWebDriver _driver)
        {
            this._driver = _driver;

            agregarUsuarioInformacionPersonalControllers = new AgregarUsuarioInformacionPersonalControllers(_driver);
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        public AgregarUsuarioInfoSistema LlenarLosCamposInfoPersonal(InformacionPersonalDto informacionPersonalDto)
        {
            agregarUsuarioInformacionPersonalControllers.campoNombre.SendKeys(informacionPersonalDto.Nombre);
            generarEvidencias.ImprimirMensaje("Nombre: " + informacionPersonalDto.Nombre);

            agregarUsuarioInformacionPersonalControllers.campoApellido.SendKeys(informacionPersonalDto.Apellido);
            generarEvidencias.ImprimirMensaje("Apellido: " + informacionPersonalDto.Apellido);

            agregarUsuarioInformacionPersonalControllers.campoEmail.SendKeys(informacionPersonalDto.Email);
            generarEvidencias.ImprimirMensaje("E-mail: " + informacionPersonalDto.Email);

            generarEvidencias.CapturarPantalla();

            return new AgregarUsuarioInfoSistema(_driver);
        }
    }
}
