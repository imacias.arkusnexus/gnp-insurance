﻿using OpenQA.Selenium;
using Utilerias;
using Controllers;

namespace PageObjectModel
{
    public class ConfigurarUsuarios
    {
        private IWebDriver _driver;

        private ConfigurarUsuariosControllers configurarUsuariosControllers;
        private FuncionesDeSelenium funcionesDeSelenium;
        private GenerarEvidencias generarEvidencias;

        public ConfigurarUsuarios(IWebDriver _driver)
        {
            this._driver = _driver;
            
            configurarUsuariosControllers = new ConfigurarUsuariosControllers(_driver);
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        /*Método*/
        public AgregarUsuarioInfoPersonal PresionarElLinkDeAgregarUsuario()
        {
            try
            {
                generarEvidencias.ImprimirMensaje("Opción seleccionada: " + configurarUsuariosControllers.linkDeAgregarUsuario.Text);
                configurarUsuariosControllers.linkDeAgregarUsuario.Click();

                generarEvidencias.CapturarPantalla();
            }

            catch(System.NullReferenceException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return new AgregarUsuarioInfoPersonal(_driver);
        }

        public void BuscarAlgunaOpcionDesdeConfigurarUsuarios()
        {
            configurarUsuariosControllers.iconoDeBuscar.Click();
        }
    }
}
