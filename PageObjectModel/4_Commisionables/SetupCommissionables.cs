﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilerias;
using ControllersConstantes;
using Controllers;

namespace PageObjectModel
{
    public class SetupCommissionables
    {
        private IWebDriver _driver;
        private SetupCommissionableControllers _setupCommissionableControllers;
        private GenerarEvidencias _generarEvidencias;
        private FuncionesDeSelenium _funcionesDeSelenium;

        public SetupCommissionables(IWebDriver _driver)
        {
            this._driver = _driver;
            _setupCommissionableControllers = new SetupCommissionableControllers(_driver);
            _generarEvidencias = new GenerarEvidencias(_driver);
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public AddCommissionableInfo ClickOnAddCommissionableLink()
        {
            try
            {
                _setupCommissionableControllers.addCommissionableLink.Click();              
                _generarEvidencias.ImprimirMensaje("Button clicked: ");
                _generarEvidencias.CapturarPantalla();
            }

            catch (NullReferenceException ex)
            {
                _generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return new AddCommissionableInfo(_driver);
        }
        
    }
}
