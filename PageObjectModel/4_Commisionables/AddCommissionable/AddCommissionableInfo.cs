﻿using OpenQA.Selenium;
using Utilerias;
using Controllers;

namespace PageObjectModel
{
    public class AddCommissionableInfo
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium _funcionesDeSelenium;
        private GenerarEvidencias _generarEvidencias;
        private AddCommissionableInfoControllers _addCommissionableInfoControllers;

        public AddCommissionableInfo(IWebDriver _driver)
        {
            this._driver = _driver;
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            _generarEvidencias = new GenerarEvidencias(_driver);
            _addCommissionableInfoControllers = new AddCommissionableInfoControllers(_driver);
        }

        public void FillAllFieldOfCommissionableInfo()
        {
            _addCommissionableInfoControllers.commissionableNameField.SendKeys("1232");
            _generarEvidencias.ImprimirMensaje("Hello World");
        }

        public void ClickOnSaveButton()
        {

        }
    }
}
