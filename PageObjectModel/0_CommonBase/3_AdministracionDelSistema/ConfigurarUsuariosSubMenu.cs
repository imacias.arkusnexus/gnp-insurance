﻿using OpenQA.Selenium;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class ConfigurarUsuariosSubMenu
    {
        private IWebDriver _driver;

        private AdministracionDelSistemaSubMenuControllers administracionDelSistemaSubMenuControllers;
        private GenerarEvidencias generarEvidencias;

        /*Constructor*/
        public ConfigurarUsuariosSubMenu(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Funciones*/
        public ConfigurarUsuarios ClickEnConfigurarUsuarios()
        {
            administracionDelSistemaSubMenuControllers = new AdministracionDelSistemaSubMenuControllers(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);

            administracionDelSistemaSubMenuControllers.opcionConfigurarUsuarios.Click();
            generarEvidencias.ImprimirMensaje("Opción Seleccionada: " + administracionDelSistemaSubMenuControllers.opcionConfigurarUsuarios.Text);
            generarEvidencias.CapturarPantalla();

            return new ConfigurarUsuarios(_driver);
        }
    }
}
