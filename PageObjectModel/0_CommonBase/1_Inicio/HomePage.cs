﻿using OpenQA.Selenium;
using Protractor;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class HomePage
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private GenerarEvidencias evidencias;
        private InicioControllers inicio;

        /*Constructor*/
        public HomePage(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);
        }

        /*Method*/
        public CommercialTruckDatosDeLaPoliza ClickEnElProgramaDeCommercialTruck()
        {
            inicio = new InicioControllers(_driver);
            evidencias = new GenerarEvidencias(_driver);

            inicio.emisorCommercialTruck.Click();
            evidencias.ImprimirMensaje("Programa seleccionado: Commercial Truck");

           return new CommercialTruckDatosDeLaPoliza(_driver);
        }
    }
}
