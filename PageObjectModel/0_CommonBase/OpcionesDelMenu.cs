﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using Controllers;
using Utilerias; 

namespace PageObjectModel
{
    public class OpcionesDelMenu
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private OpcionesDelMenuControllers opcionesMenu;
        private GenerarEvidencias evidencias;

        /*Constructor*/
        public OpcionesDelMenu(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);
        }

        /*Method*/
        public void OpcionesMenu(OperacionesMenu.Opciones opciones)
        {
            opcionesMenu = new OpcionesDelMenuControllers(_driver);
            evidencias = new GenerarEvidencias(_driver);

            switch (opciones)
            {
                case OperacionesMenu.Opciones.Autoservicio:
                    opcionesMenu.clickEnAutoservicio.Click();
                    evidencias.ImprimirMensaje("Opción seleccionada: " + opcionesMenu.clickEnAutoservicio.Text);
               break;

               case OperacionesMenu.Opciones.AdministracionDelSistema:
                    opcionesMenu.clickEnAdministracionDelSistema.Click();
                    evidencias.ImprimirMensaje("Opción seleccionada: " + opcionesMenu.clickEnAdministracionDelSistema.Text);
               break;

                case OperacionesMenu.Opciones.Comisionables:
                    opcionesMenu.clickEnComisionables.Click();
                    evidencias.ImprimirMensaje("Opción seleccionada: " + opcionesMenu.clickEnComisionables.Text);
                break;

                case OperacionesMenu.Opciones.FuncionesDeLaAgencia:
                    opcionesMenu.clickEnFuncionesDeLaAgencia.Click();
                    evidencias.ImprimirMensaje("Opción seleccionada: " + opcionesMenu.clickEnFuncionesDeLaAgencia.Text);
                break;

                case OperacionesMenu.Opciones.PagosLiquidaciones:
                    break;

                case OperacionesMenu.Opciones.ReportesDeLaAgencia:
                    break;
            }          
        }
    }
}
