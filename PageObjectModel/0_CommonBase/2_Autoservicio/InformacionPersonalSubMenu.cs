﻿using OpenQA.Selenium;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class InformacionPersonalSubMenu
    {
        private IWebDriver _driver;

        private AutoservicioSubMenuControllers autoservicioSubMenuControllers;
        private GenerarEvidencias generarEvidencias;

        public InformacionPersonalSubMenu(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        public InformacionPersonal ClickEnLaOpcionDeInformacionPersonal()
        {
            autoservicioSubMenuControllers = new AutoservicioSubMenuControllers(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);

            autoservicioSubMenuControllers.opcionDeInfoPersonal.Click();
            generarEvidencias.ImprimirMensaje("Opción Seleccionada: " + autoservicioSubMenuControllers.opcionDeInfoPersonal.Text);
            generarEvidencias.CapturarPantalla();

            return new InformacionPersonal(_driver);
        }
    }
}
