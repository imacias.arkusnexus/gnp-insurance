﻿using OpenQA.Selenium;
using Protractor;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class CambiarClaveSubMenu
    {
        private IWebDriver _driver;
        private NgWebDriver _ngDriver;
        private AutoservicioSubMenuControllers cambiarClaveSubMenu;

        /*Constructor*/
        public CambiarClaveSubMenu(IWebDriver _driver)
        {
            this._driver = _driver;
            _ngDriver = new NgWebDriver(_driver);

            cambiarClaveSubMenu = new AutoservicioSubMenuControllers(_driver);
        }
        
        /*Metodos*/
        public CambiarClave ClickEnLaOpcionDeCambiarClave()
        {
            GenerarEvidencias generarEvidencia = new GenerarEvidencias(_driver);

            try
            {
                cambiarClaveSubMenu.opcionDeCambiarClave.Click();
                generarEvidencia.ImprimirMensaje("Opción seleccionada: " + cambiarClaveSubMenu.opcionDeCambiarClave.Text);
            }

            catch (ElementNotVisibleException ex)
            {
                generarEvidencia.ImprimirMensaje(ex.Message);
            }

           return new CambiarClave(_driver);
        }

    }
}
