﻿using Controllers;
using OpenQA.Selenium;
using Utilerias;

namespace PageObjectModel
{
    public class SetupCommissionableSubMenu
    {
        private IWebDriver _driver;
        private GenerarEvidencias _generarEvidencias;
        private ComissionableSubMenuControllers _comissionableSubMenuControllers;

        public SetupCommissionableSubMenu(IWebDriver _driver)
        {
            this._driver = _driver;
            _generarEvidencias = new GenerarEvidencias(_driver);
            _comissionableSubMenuControllers = new ComissionableSubMenuControllers(_driver);
        }

        public SetupCommissionables ClickOnSetupCommissionable()
        {
            _comissionableSubMenuControllers.clickOnSetupCommissionables.Click();
            _generarEvidencias.ImprimirMensaje("Opción Seleccionada: " + _comissionableSubMenuControllers.clickOnSetupCommissionables.Text);
            _generarEvidencias.CapturarPantalla();

            return new SetupCommissionables(_driver);
        }

        public void ClickOnAssignCommissionable()
        {
            _comissionableSubMenuControllers.clickOnAssignCommissionable.Click();
            _generarEvidencias.ImprimirMensaje(_comissionableSubMenuControllers.clickOnAssignCommissionable.Text);
            _generarEvidencias.CapturarPantalla();
        }
    }
}
