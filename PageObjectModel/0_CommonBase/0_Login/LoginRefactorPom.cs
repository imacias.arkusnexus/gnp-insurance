﻿using OpenQA.Selenium;
using System;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class LoginRefactorPom
    {
        private IWebDriver _driver;
        private LoginControllers login;

        /*Constructor*/
        public LoginRefactorPom(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Method*/
        public OpcionesDelMenu EscribirCredenciales(Credenciales credenciales)
        {
            try
            {
                GenerarEvidencias generarEvidencia = new GenerarEvidencias(_driver);
                generarEvidencia.CapturarPantalla();

                login = new LoginControllers(_driver);

                login.campoDeUsuario.SendKeys(credenciales.Usuario);
                generarEvidencia.ImprimirMensaje("Usuario: " + credenciales.Usuario);

                login.campoDeContrasena.SendKeys(credenciales.Contrasena);
                generarEvidencia.ImprimirMensaje("Contraseña: " + credenciales.Contrasena);
                                
                login.botonDeLogin.Click();
                generarEvidencia.ImprimirMensaje("Botón presionado");
            }

            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new OpcionesDelMenu(_driver);
        }
    }
}
