﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class CommercialTruckDatosDeLaPoliza
    {
        private IWebDriver _driver;

        private DatosDeLaPolizaControllers datosPoliza;
        private GenerarEvidencias generarEvidencias;
        private FuncionesDeSelenium funcionesSelenium;

        /*Constructor*/
        public CommercialTruckDatosDeLaPoliza(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Methods*/
        public InformacionDelAsegurado LlenarPestanaDatosDeLaPoliza(CommercialTruckDto commercialTruckDto)
        {
            datosPoliza = new DatosDeLaPolizaControllers(_driver);

            generarEvidencias = new GenerarEvidencias(_driver);
            funcionesSelenium = new FuncionesDeSelenium(_driver);

            try
            {
                generarEvidencias.CapturarPantalla();

                funcionesSelenium.SeleccionarDropDownList(datosPoliza.campoEstadoEntrada, commercialTruckDto.EstadoDeEntrada);
                generarEvidencias.ImprimirMensaje("Estado de entrada: " + commercialTruckDto.EstadoDeEntrada.ToString());

                funcionesSelenium.SeleccionarDropDownList(datosPoliza.campoCobertura, commercialTruckDto.Cobertura);
                generarEvidencias.ImprimirMensaje("Cobertura: " + 
                    commercialTruckDto.Cobertura.ToString());

                funcionesSelenium.SeleccionarDropDownList(datosPoliza.campoHora, commercialTruckDto.Hora);
                generarEvidencias.ImprimirMensaje("Hora: " + commercialTruckDto.Hora.ToString());

                funcionesSelenium.SeleccionarDropDownList(datosPoliza.campoMinuto, commercialTruckDto.Minuto);
                generarEvidencias.ImprimirMensaje("Minuto: " + commercialTruckDto.Minuto.ToString());

                datosPoliza.campoCalendario.SendKeys(commercialTruckDto.Fecha);
                generarEvidencias.ImprimirMensaje("Fecha: " + commercialTruckDto.Fecha.ToString());
            }

            catch (NoSuchElementException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return new InformacionDelAsegurado(_driver);
        }
    }
}
