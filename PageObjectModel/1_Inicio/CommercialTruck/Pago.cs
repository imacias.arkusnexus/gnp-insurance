﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class Pago
    {
        private IWebDriver _driver;

        private PagoControllers pagoControllers;
      
        private FuncionesDeSelenium funcionesDeSelenium;
        private GenerarEvidencias generarEvidencias;
   
        /*Constructor*/
        public Pago(IWebDriver _driver)
        {
            this._driver = _driver;            
        }

        /*Metodo*/
        public void PagarPolizaDeCommercialTruck(PagoDto pagoDto)
        {
            pagoControllers = new PagoControllers(_driver);

            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
            
            funcionesDeSelenium.SeleccionarDropDownList(pagoControllers.seleccionarOpcionDePago, pagoDto.Pago);
            generarEvidencias.ImprimirMensaje("Opción de pago: " + pagoDto.Pago);
            generarEvidencias.CapturarPantalla();
        }

        public void VerificarInformacionDeLaPolizaGenerada()
        {
            string windows = _driver.CurrentWindowHandle;
            _driver.SwitchTo().Window(windows);

            generarEvidencias.ImprimirMensaje("Poliza generada :D");
           
        }
    }
}
