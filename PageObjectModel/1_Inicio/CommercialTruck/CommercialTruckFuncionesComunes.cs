﻿using OpenQA.Selenium;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class CommercialTruckFuncionesComunes
    {
        private IWebDriver _driver;
        public CommercialTruckFuncionesComunesControllers commercialTruckFuncionesComunesControllers;
        public GenerarEvidencias generarEvidencias;

        /*Constructor*/
        public CommercialTruckFuncionesComunes(IWebDriver _driver)
        {
            this._driver = _driver;
            commercialTruckFuncionesComunesControllers = new CommercialTruckFuncionesComunesControllers(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);
        }

        public void ClickEnElBotonDeAnterior()
        {
            commercialTruckFuncionesComunesControllers.botonDeAnterior.Click();
            generarEvidencias.ImprimirMensaje("Botón: " + commercialTruckFuncionesComunesControllers.botonDeAnterior.Text);
        }

        public void ClickEnElBotonDeContinuar()
        {
            commercialTruckFuncionesComunesControllers.botonDeContinuar.Click();
            generarEvidencias.ImprimirMensaje("Botón: " + commercialTruckFuncionesComunesControllers.botonDeContinuar.Text);
        }

        public void ClickEnElBotonDeGuardar()
        {
            commercialTruckFuncionesComunesControllers.botonDeGrabar.Click();
            generarEvidencias.ImprimirMensaje("Botón: " + commercialTruckFuncionesComunesControllers.botonDeGrabar.Text);
        }

        public void ClickEnElBotonDeCancelar()
        {
            commercialTruckFuncionesComunesControllers.botonDeCancelar.Click();
            generarEvidencias.ImprimirMensaje("Botón: " + commercialTruckFuncionesComunesControllers.botonDeCancelar.Text);
        }

        public void ClickEnElBotonDeCerrar()
        {
            commercialTruckFuncionesComunesControllers.botonDeCerrar.Click();
            generarEvidencias.ImprimirMensaje("Botón: " + commercialTruckFuncionesComunesControllers.botonDeCerrar.Text);
        }

        public void ClickEnBotonDeContinuarDatosPoliza()
        {
            commercialTruckFuncionesComunesControllers.botonDeContinuarDatosPoliza.Click();
            generarEvidencias.ImprimirMensaje("Continuar");
        }
    }
}
