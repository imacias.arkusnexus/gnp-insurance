﻿using OpenQA.Selenium;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class InformacionDelVehiculo
    {
        private IWebDriver _driver;

        private FuncionesDeSelenium funcionesDeSelenium;
        private GenerarEvidencias generarEvidencias;

        private InformacionDelVehiculoControllers informacionDelVehiculoControllers;
       
        /*Constructor*/
        public InformacionDelVehiculo(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        public ConductoresAdicionales LlenarLosCamposInformacionDelVehiculo(InformacionDelVehiculoDto informacionDelVehiculoDto)
        {
            try
            {
                informacionDelVehiculoControllers = new InformacionDelVehiculoControllers(_driver);

                funcionesDeSelenium = new FuncionesDeSelenium(_driver);
                generarEvidencias = new GenerarEvidencias(_driver);

                generarEvidencias.ImprimirMensaje("** INFORMACION DEL VEHICULO **");

                funcionesDeSelenium.SeleccionarDropDownList(
                    informacionDelVehiculoControllers.campoTipoVehiculo, informacionDelVehiculoDto.TipoVehiculo);
                generarEvidencias.ImprimirMensaje("Tipo de vehículo: " + informacionDelVehiculoDto.TipoVehiculo.ToString());

                funcionesDeSelenium.SeleccionarDropDownList(informacionDelVehiculoControllers.campoAnio, informacionDelVehiculoDto.Anio);
                generarEvidencias.ImprimirMensaje("Año: " + informacionDelVehiculoDto.Anio);

                informacionDelVehiculoControllers.campoMarca.SendKeys(informacionDelVehiculoDto.Marca);
                generarEvidencias.ImprimirMensaje("Marca: " + informacionDelVehiculoDto.Marca);

                informacionDelVehiculoControllers.campoModelo.SendKeys(informacionDelVehiculoDto.Modelo);
                generarEvidencias.ImprimirMensaje("Modelo: " + informacionDelVehiculoDto.Modelo);

                funcionesDeSelenium.SeleccionarDropDownList(informacionDelVehiculoControllers.campoPaisVehiculo, informacionDelVehiculoDto.Pais);
                generarEvidencias.ImprimirMensaje("País: " + informacionDelVehiculoDto.Pais);

                funcionesDeSelenium.SeleccionarDropDownList(informacionDelVehiculoControllers.campoEstadoVehiculo, informacionDelVehiculoDto.Estado);
                generarEvidencias.ImprimirMensaje("Estado: " + informacionDelVehiculoDto.Estado);

                informacionDelVehiculoControllers.campoVin.SendKeys(informacionDelVehiculoDto.NumeroVehiculo);
                generarEvidencias.ImprimirMensaje("VIN: " + informacionDelVehiculoDto.NumeroVehiculo);

                informacionDelVehiculoControllers.campoPlacas.SendKeys(informacionDelVehiculoDto.Placas);
                generarEvidencias.ImprimirMensaje("Placas: " + informacionDelVehiculoDto.Placas);

                generarEvidencias.CapturarPantalla();
            }

            catch(NoSuchElementException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return new ConductoresAdicionales(_driver);
        }
    }
}
