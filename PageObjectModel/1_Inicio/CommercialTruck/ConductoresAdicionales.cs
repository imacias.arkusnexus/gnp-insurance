﻿using OpenQA.Selenium;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class ConductoresAdicionales
    {
        private IWebDriver _driver;
        private ConductoresAdicionalesControllers conductoresAdicionalesControllers;
        private GenerarEvidencias generarEvidencias;

        public ConductoresAdicionales(IWebDriver _driver)
        {
            this._driver = _driver;
            conductoresAdicionalesControllers = new ConductoresAdicionalesControllers(_driver);

            generarEvidencias = new GenerarEvidencias(_driver);
        }

        public Pago AgregarConductoresAdicionales()
        {
            try
            {
                conductoresAdicionalesControllers.botonDeAgregar.Click();
                generarEvidencias.ImprimirMensaje("Botón: " + conductoresAdicionalesControllers.botonDeAgregar.Text);
            }

            catch(NoSuchElementException ex)
            {
                generarEvidencias.ImprimirMensaje(ex.Message);
            }

            return new Pago(_driver);
        }
    }
}
