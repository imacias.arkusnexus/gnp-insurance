﻿using OpenQA.Selenium;
using System;
using DataTransferObjects;
using Controllers;
using Utilerias;

namespace PageObjectModel
{
    public class InformacionDelAsegurado
    {
        private IWebDriver _driver;
       
        private InformacionDelAseguradoControllers informacionDelAseguradoControllers;
      
        private FuncionesDeSelenium funcionesSelenium;
        private GenerarEvidencias evidencias;

        /*Constructor*/
        public InformacionDelAsegurado(IWebDriver _driver)
        {
            this._driver = _driver;
        }
            
        /*Method*/
        public InformacionDelVehiculo LlenarLosCamposDeInformacionDelAsegurado(InformacionDelAseguradoDto informacionDelAseguradoDto)
        {
            informacionDelAseguradoControllers = new InformacionDelAseguradoControllers(_driver);

            funcionesSelenium = new FuncionesDeSelenium(_driver);
            evidencias = new GenerarEvidencias(_driver);
            
            try
            {
                evidencias.ImprimirMensaje("** INFORMACION DEL ASEGURADO **");

                informacionDelAseguradoControllers.campoNombre.SendKeys(informacionDelAseguradoDto.Nombre);
                evidencias.ImprimirMensaje("Nombre: " + informacionDelAseguradoDto.Nombre);

                informacionDelAseguradoControllers.campoTelefono.SendKeys(informacionDelAseguradoDto.Telefono);
                evidencias.ImprimirMensaje("Teléfono: " + informacionDelAseguradoDto.Telefono);

                funcionesSelenium.SeleccionarDropDownList(informacionDelAseguradoControllers.campoPais, informacionDelAseguradoDto.Pais);
                evidencias.ImprimirMensaje("País: " + informacionDelAseguradoDto.Pais.ToString());

                funcionesSelenium.SeleccionarDropDownList(informacionDelAseguradoControllers.campoEstado, informacionDelAseguradoDto.Estado);
                evidencias.ImprimirMensaje("Estado: " + informacionDelAseguradoDto.Estado.ToString());

                informacionDelAseguradoControllers.campoCalle.SendKeys(informacionDelAseguradoDto.Calle);
                evidencias.ImprimirMensaje("Calle: " + informacionDelAseguradoDto.Calle);

                informacionDelAseguradoControllers.campoColonia.SendKeys(informacionDelAseguradoDto.Colonia);
                evidencias.ImprimirMensaje("Colonia: " + informacionDelAseguradoDto.Colonia);

                informacionDelAseguradoControllers.campoCiudad.SendKeys(informacionDelAseguradoDto.Ciudad);
                evidencias.ImprimirMensaje("Ciudad: " + informacionDelAseguradoDto.Ciudad);

                informacionDelAseguradoControllers.campoNumero.SendKeys(informacionDelAseguradoDto.Numero);
                evidencias.ImprimirMensaje("Número: " + informacionDelAseguradoDto.Numero);

                informacionDelAseguradoControllers.campoCorreoElectronico.SendKeys(informacionDelAseguradoDto.CorreoElectronico);
                evidencias.ImprimirMensaje("Correo: " + informacionDelAseguradoDto.CorreoElectronico);

                evidencias.CapturarPantalla();
            }

            catch(NoSuchElementException ex)
            {
                evidencias.ImprimirMensaje(ex.Message);
            }

            catch(ArgumentNullException ex)
            {
                evidencias.ImprimirMensaje(ex.Message);
            }

            return new InformacionDelVehiculo(_driver);
        }
    }
}
