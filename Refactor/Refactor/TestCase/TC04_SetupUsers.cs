﻿using NUnit.Framework;
using TestCase;
using Enum;
using DataTransferObjects;
using ControllersConstantes;
using PageObjectModel;

namespace Refactor
{
    public class TC04_SetupUsers : TestBaseRefactor
    {
        public TC04_SetupUsers() : base(Navegadores.Navegador.Chrome)
        {

        }

        [Test, Description("Crear un nuevo usuario")]
        public void CrearUnNuevoUsuario()
        {
            /*
            * STEPS TO REPRODUCE:
            * 1. Open the URL: http://nexusinsuranceserver:8081/UI/
            * 2. Log in with the credentials:
            * 3. Go to menu, click on "System Administrator" option.
            * 4. Click on "Setup Users" sub-option.
            * 5. Click on "Add users" link.
            * 6. On "Personal Info." tab, fill the fields:
            *    - Name
            *    - Last name
            *    - Email
            *    - Address
            *    - City
            * 7. Click on "System Info" tab.
            * 8. Fill out all fields:
            *    - Username
            *    - Password
            *    - Confirm Password
            *    - Language
            * 9. Click on "Save" button.
            */  
            
           LoginRefactorPom login = new LoginRefactorPom(_driver);
           OpcionesDelMenu opcionesMenu = login.EscribirCredenciales(LoginConstantes.AdminQuic);
           opcionesMenu.OpcionesMenu(OperacionesMenu.Opciones.AdministracionDelSistema);

           ConfigurarUsuariosSubMenu configurarUsuariosSubMenu = new ConfigurarUsuariosSubMenu(_driver);
           ConfigurarUsuarios configurarUsuarios = configurarUsuariosSubMenu.ClickEnConfigurarUsuarios();

           InformacionPersonalDto personalInfoDto = new InformacionPersonalDto()
           {
               Nombre = "Ramona",
               Apellido = "Acosta",
               Email = "racosta@mailinator.com",
               Direccion = "Calle Flores Magallón",
               Ciudad = "Torreón"
           };

           AgregarUsuarioInfoPersonal agregarUsuarioInfoPersonal = configurarUsuarios.PresionarElLinkDeAgregarUsuario();
           AgregarUsuarioInfoSistema agregarUsuarioInfoSistema = agregarUsuarioInfoPersonal.LlenarLosCamposInfoPersonal(personalInfoDto);

           AgregarUsuarioInformacionSistemaDto informacionSistemaEntity = new AgregarUsuarioInformacionSistemaDto()
           {
               Usuario = "racosta",
               Contrasena = "Qwerty2@",
               ConfirmarContrasena = "Qwerty2@",
               Idioma = "Inglés"
           };

           agregarUsuarioInfoSistema.ClickEnLaPestanaDeInfoSistema();
           agregarUsuarioInfoSistema.LlenarLosCamposDeInfoSistema(informacionSistemaEntity);

           AgregarUsuariosFuncionesGenerales agregarUsuariosFuncionesGenerales = new AgregarUsuariosFuncionesGenerales(_driver);
           agregarUsuariosFuncionesGenerales.ClickEnElBotonDeGrabar();            
       }

       [Test, Description("Search a value successfully.")]
       public void VerifyGridSetupUsers()
       {
            /*
             * STEPS TO REPRODUCE:
             * 1. Open the URL: http://nexusinsuranceserver:8081/UI/
             * 2. Log in with the credentials:
             * 3. Go to menu, click on "System Administrator" option.
             * 4. Click on "Setup Users" sub-option.
             * 5. On "Search" field, search by name.
             * 6. Type the value on "Search Value" field.
             * 7. Click on magnifying glass icon to searh the element.
             * 8. Change the option from "Search" drop-down list to "None".
             * 9. Click on magnifying glass icon to search the element.
             * 10. Sort "Email" column descending.
             */

            LoginRefactorPom login = new LoginRefactorPom(_driver);
            OpcionesDelMenu opcionesMenu = login.EscribirCredenciales(LoginConstantes.AdminQuic);
            opcionesMenu.OpcionesMenu(OperacionesMenu.Opciones.AdministracionDelSistema);

            ConfigurarUsuariosSubMenu configurarUsuariosSubMenu = new ConfigurarUsuariosSubMenu(_driver);
            ConfigurarUsuarios configurarUsuarios = configurarUsuariosSubMenu.ClickEnConfigurarUsuarios();

            SetupUsersDto setupUsersDto = new SetupUsersDto()
            {
                Search = "Nombre",
                SearchValue = "Ramona"
            };

            SetupUserGrid setupUserGrid = new SetupUserGrid(_driver);
            setupUserGrid.SelectAnOptionToSearch(setupUsersDto);
            setupUserGrid.TypeASearchValue(setupUsersDto);
            setupUserGrid.ClickOnMagnifyingGlassIcon();

            SetupUsersDto setupUsersDto01 = new SetupUsersDto()
            {
                Search = "Ninguno",
                SearchValue = "Ramona"
            };

            setupUserGrid.SelectAnOptionToSearch(setupUsersDto01);
            setupUserGrid.ClickOnMagnifyingGlassIcon();
            setupUserGrid.SortColumnsAscendingDescending();
        }

        [Test, Description("Search a value, edit any user")]
        public void SearchValueAndEditAnUser()
        {
            /*Pre-condition:
             *  - Execute the previous Test Case.
             *
             */
            VerifyGridSetupUsers();

            SetupUserGrid setupUserGrid = new SetupUserGrid(_driver);
            setupUserGrid.EditAnUser();
        }
    }
}
