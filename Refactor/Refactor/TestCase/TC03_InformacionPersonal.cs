﻿using NUnit.Framework;
using TestCase;
using Enum;
using DataTransferObjects;
using PageObjectModel;

namespace Refactor
{
    public class TC03_InformacionPersonal : TestBaseRefactor
    {
        public TC03_InformacionPersonal() : base(Navegadores.Navegador.Chrome)
        {

        }

        [Test, Description("Llenar los campos de Información Personal")]
        public void LlenarInfoPersonal()
        {
            LoginRefactorPom login = new LoginRefactorPom(_driver);
            OpcionesDelMenu opcionesDelMenu = login.EscribirCredenciales(CambiarClaveConstantes.AdminQuicQa);
            opcionesDelMenu.OpcionesMenu(OperacionesMenu.Opciones.Autoservicio);
            
            InformacionPersonalSubMenu informacionPersonalSubMenu = new InformacionPersonalSubMenu(_driver);
            InformacionPersonal informacionPersonal = informacionPersonalSubMenu.ClickEnLaOpcionDeInformacionPersonal();

            InformacionPersonalDto informacionPersonalDto = new InformacionPersonalDto()
            {
                Nombre = "ABC",
                Apellido = "Galaviz",
                Email = "admin@mailinator.com",
                Ciudad = "Tijuana",
                Telefono = "6641234789",
                Idioma = "Inglés"
            };

            informacionPersonal.LlenarInfoPersonal(informacionPersonalDto);
        }
    }
}
