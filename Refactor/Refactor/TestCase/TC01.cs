﻿using NUnit.Framework;
using TestCase;
using Enum;
using PageObjectModel;

namespace Refactor
{
    public class TC01 : TestBaseRefactor
    {
        public TC01() : base(Navegadores.Navegador.Chrome)
        {

        }

        [Test, Description("Cambiar las contraseñas exitósamente")]
        public void IniciarSesion()
        {
            LoginRefactorPom login = new LoginRefactorPom(_driver);
            OpcionesDelMenu menu = login.EscribirCredenciales(CambiarClaveConstantes.AdminQuicQa);

            menu.OpcionesMenu(OperacionesMenu.Opciones.Autoservicio);

            CambiarClaveSubMenu opciondeCambiarClave = new CambiarClaveSubMenu(_driver);

            CambiarClave clave = opciondeCambiarClave.ClickEnLaOpcionDeCambiarClave();
            clave.CambiarLaClaveDelSistema(CambiarClaveConstantes.NuevaClave, CambiarClaveConstantes.ConfirmarClave);
        }

        [Test, Description("Cambiar las contraseñas.")]
        public void PresionarElBotonDeGuardarSinHacerCambios()
        {
            LoginRefactorPom login = new LoginRefactorPom(_driver);
            OpcionesDelMenu menu = login.EscribirCredenciales(CambiarClaveConstantes.AdminQuicQa);
           // OpcionesDelMenu menu = new OpcionesDelMenu(_driver);
            menu.OpcionesMenu(OperacionesMenu.Opciones.Autoservicio);

            CambiarClaveSubMenu opciondeCambiarClave = new CambiarClaveSubMenu(_driver);

            CambiarClave clave = opciondeCambiarClave.ClickEnLaOpcionDeCambiarClave();
            clave.CambiarLaClaveDelSistema("42784278", "42784278");
        }
    }
}
