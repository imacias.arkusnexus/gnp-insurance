﻿using NUnit.Framework;
using TestCase;
using Enum;
using DataTransferObjects;
using PageObjectModel;

namespace Refactor 
{
    public class TC02_CommercialTruck : TestBaseRefactor
    {
        public TC02_CommercialTruck() : base(Navegadores.Navegador.Chrome)
        {
            
        }

        [Test, Description("Commercial Truck: Comprar una póliza de CommercialTruck")]
        public void LlenarPolizaDeComemcialTruck()
        {
            CommercialTruckFuncionesComunes commercialTruckFuncionesComunes = new CommercialTruckFuncionesComunes(_driver);

            LoginRefactorPom login = new LoginRefactorPom(_driver);
            OpcionesDelMenu menu = login.EscribirCredenciales(CambiarClaveConstantes.AdminQuicQa);
            
            //Se invoca el objeto de la entity con el valor deseado.
            CommercialTruckDto commercialTruck = new CommercialTruckDto()
            {
                EstadoDeEntrada = "ARIZONA", //"1: 3", 
                Cobertura = "$1,000,000 CSL", //"1: 350", 
                Hora = "19", //"19: 19",
                Minuto = "45", //"3: 45",
                Fecha = "06/30/2019",

                Asegurado = new InformacionDelAseguradoDto()
                {
                    Nombre = "Karem Flores",
                    Telefono = "66412345678",
                    Pais = "México", //"1: 136",
                    Estado = " BAJA CALIFORNIA ", //"2: 66", 
                    Calle = "Calle Benito Juárez",
                    Colonia = "Universidad",
                    Ciudad = "Tijuana",
                    Numero = "1234",
                    CorreoElectronico = "imacias@arkusnexus.com"
                },

                Vehiculo = new InformacionDelVehiculoDto()
                {
                    TipoVehiculo = "Commercial Truck",
                    Anio = "2019", //2: 2019
                    Marca = "Toyota",
                    Modelo = "Corolla",
                    Pais = "México",
                    Estado = " BAJA CALIFORNIA ",
                    NumeroVehiculo = "A1S2D3F4G5H6J7K8L",
                    Placas = "QWERT56",
                    PuertosEntrada = "All Ports" //"1: All Ports"
                }
            };

            PagoDto pagoDto = new PagoDto()
            {
                Pago = "Efectivo" //"1: 0"
            };

            menu.OpcionesMenu(OperacionesMenu.Opciones.Inicio);

            HomePage homePage = new HomePage(_driver);
            CommercialTruckDatosDeLaPoliza datosDeLaPoliza = 
                homePage.ClickEnElProgramaDeCommercialTruck();
            InformacionDelAsegurado informacionDelAsegurado = 
                datosDeLaPoliza.LlenarPestanaDatosDeLaPoliza(commercialTruck);
            commercialTruckFuncionesComunes.ClickEnBotonDeContinuarDatosPoliza();
          
            InformacionDelVehiculo informacionDelVehiculo = 
                informacionDelAsegurado.LlenarLosCamposDeInformacionDelAsegurado(commercialTruck.Asegurado);
            ConductoresAdicionales conductoresAdicionales = informacionDelVehiculo.LlenarLosCamposInformacionDelVehiculo(commercialTruck.Vehiculo);

            for(int i=0; i<=1; i++)
            {
                commercialTruckFuncionesComunes.ClickEnElBotonDeContinuar();
            }

            Pago pago = new Pago(_driver);
            pago.PagarPolizaDeCommercialTruck(pagoDto);
            
            commercialTruckFuncionesComunes.ClickEnElBotonDeContinuar();
            pago.VerificarInformacionDeLaPolizaGenerada();
        }
    }
}
