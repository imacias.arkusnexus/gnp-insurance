﻿using NUnit.Framework;
using OpenQA.Selenium;
using Protractor;
using System;
using Enum;
using Utilerias;
using PageObjectModel;
using ControllersConstantes;

namespace TestCase
{
    public class TestBaseRefactor
    {
        protected IWebDriver _driver;
        protected NgWebDriver ngDriver;
        protected CrossBrowser crossBrowser;

        protected GenerarEvidencias generarEvidencias;
        protected LoginRefactorPom login;

        /*Constructor*/
        public TestBaseRefactor(Navegadores.Navegador navegador)
        {
            crossBrowser = new CrossBrowser(_driver);
            generarEvidencias = new GenerarEvidencias(_driver);


            _driver = crossBrowser.Driver(navegador);
            if (_driver == default(IWebDriver))
            {
                generarEvidencias.ImprimirMensaje("El navegador seleccionado no está disponible.");
            }
        }

        [SetUp]
        public void Setup()
        {
            _driver.Manage().Cookies.DeleteAllCookies();

            // Configure timeouts (important since Protractor uses asynchronous client side scripts)
            _driver.Manage().Timeouts().AsynchronousJavaScript = TimeSpan.FromSeconds(60);
            
            //Meet the trick here
            ngDriver = new NgWebDriver(_driver);

            _driver.Navigate().GoToUrl("http://nexusinsuranceserver:8081/UI/#/login/");
            generarEvidencias.ImprimirMensaje("URL: http://nexusinsuranceserver:8081/UI/#/login/");

            ngDriver.Manage().Window.Maximize();
            generarEvidencias.ImprimirMensaje("Se maximizó la pantalla");
        }

        [TearDown]
        public void CloseBrowser()
        {
            ngDriver.Close();
            ngDriver.Quit(); //Close all tabs.
        }
    }
}
