﻿using TestCase;
using Enum;
using NUnit.Framework;
using PageObjectModel;
using ControllersConstantes;

namespace Refactor
{
    public class TC05_ComissionableSetup : TestBaseRefactor
    {
        public TC05_ComissionableSetup() : base(Navegadores.Navegador.Chrome)
        {

        }

        [Test, Description("Create a new commissionable")]
        public void CreateANewCommissionable()
        {
            LoginRefactorPom login = new LoginRefactorPom(_driver);
            login.EscribirCredenciales(LoginConstantes.AdminQuic);

            OpcionesDelMenu opcionesDelMenu = new OpcionesDelMenu(_driver);
            opcionesDelMenu.OpcionesMenu(OperacionesMenu.Opciones.Comisionables);

            SetupCommissionableSubMenu setupCommissionableSubMenu = new SetupCommissionableSubMenu(_driver);
            SetupCommissionables setupCommissionables = setupCommissionableSubMenu.ClickOnSetupCommissionable();
            AddCommissionableInfo addCommissionableInfo =            setupCommissionables.ClickOnAddCommissionableLink();
            addCommissionableInfo.FillAllFieldOfCommissionableInfo();
        }

        [Test, Description("Assign a commissionable")]
        public void AssignCommissionableSuccessfully()
        {
            LoginRefactorPom login = new LoginRefactorPom(_driver);
            login.EscribirCredenciales(LoginConstantes.AdminQuic);

            OpcionesDelMenu opcionesDelMenu = new OpcionesDelMenu(_driver);
            opcionesDelMenu.OpcionesMenu(OperacionesMenu.Opciones.Comisionables);

            SetupCommissionableSubMenu setupCommissionable = new SetupCommissionableSubMenu(_driver);
            setupCommissionable.ClickOnAssignCommissionable();
        }
    }
}
