﻿namespace DataTransferObjects
{
    public class Credenciales
    {
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
    }
}
