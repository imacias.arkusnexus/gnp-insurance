﻿namespace DataTransferObjects
{
    public class SetupUsersDto
    {
        public string Search { get; set; }
        public string SearchValue { get; set; }
    }
}
