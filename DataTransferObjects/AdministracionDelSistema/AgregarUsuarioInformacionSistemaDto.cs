﻿namespace DataTransferObjects
{
    public class AgregarUsuarioInformacionSistemaDto
    {
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public string ConfirmarContrasena { get; set; }
        public string Idioma { get; set; }
    }
}
