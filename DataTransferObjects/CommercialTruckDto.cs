﻿namespace DataTransferObjects
{
    public class CommercialTruckDto
    {
        public string EstadoDeEntrada { get; set; }
        public string Cobertura { get; set; }
        public string Hora { get; set; }
        public string Minuto { get; set; }
        public string Fecha { get; set; }

        public InformacionDelAseguradoDto Asegurado { get; set; }
        public InformacionDelVehiculoDto Vehiculo { get; set; }

    }
}
