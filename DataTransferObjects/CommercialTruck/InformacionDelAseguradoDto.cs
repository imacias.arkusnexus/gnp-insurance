﻿namespace DataTransferObjects
{
    public class InformacionDelAseguradoDto
    {
        /*Información del asegurado*/
        public string Nombre { get; set; }
        public string Rfc { get; set; }
        public string Arrendatario { get; set; }
        public string Telefono { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string Calle { get; set; }
        public string Colonia { get; set; }
        public string Ciudad { get; set; }
        public string Numero { get; set; }
        public string CodigoPostal { get; set; }
        public string CorreoElectronico { get; set; }
    }
}
