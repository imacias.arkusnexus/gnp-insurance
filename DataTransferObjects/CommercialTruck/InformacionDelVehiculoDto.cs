﻿namespace DataTransferObjects
{
    public class InformacionDelVehiculoDto
    {
        /*Información del vehículo*/
        public string TipoVehiculo { get; set; }
        public string Anio { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string NumeroVehiculo { get; set; }
        public string Placas { get; set; }
        public string PuertosEntrada { get; set; }
        public string Caja { get; set; }
    }
}
