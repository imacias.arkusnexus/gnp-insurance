﻿namespace DataTransferObjects
{
    public class ConductoresAdicionalesDto
    {
        public string Nombre { get; set; }
        public string Licencia { get; set; }
        public string FechaNacimiento { get; set; }
        public string Ocupacion { get; set; }
    }
}
