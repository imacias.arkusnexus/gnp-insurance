﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class OpcionesDelMenuControllers
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private FuncionesDeSelenium funcionesDeSelenium;
        
        /*Constructor*/
        public OpcionesDelMenuControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);

            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement clickEnAutoservicio
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.Autoservicio);
            }
        }

        public IWebElement clickEnAdministracionDelSistema
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.AdministracionDelSistema);
            }
        }

        public IWebElement clickEnComisionables
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.Comisionable);
            }
        }

        public IWebElement clickEnFuncionesDeLaAgencia
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.FuncionesDeLaAgencia); 
            }
        }

        public IWebElement clickEnPagosLiquidaciones
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.PagosLiquidaciones); 
            }
        }

        public IWebElement clickEnReportesDeLaAgencia
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, OpcionesDelMenuControllersConstantes.ReportesDeLaAgencia); 
            }
        }
    }
}
