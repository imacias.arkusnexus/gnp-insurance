﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class ConfigurarUsuariosControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;
  
        public ConfigurarUsuariosControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement linkDeAgregarUsuario
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, ConfigurarUsuariosControllersConstantes.AgregarUsuarioId);
            }
        }

        public IWebElement campoDeBusqueda
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, ConfigurarUsuariosControllersConstantes.Busqueda);
            }
        }

        public IWebElement campoDeValorDeBusqueda
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, ConfigurarUsuariosControllersConstantes.ValorDeBusqueda);
            }
        }

        public IWebElement iconoDeBuscar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, ConfigurarUsuariosControllersConstantes.IconoDeBuscar);
            }
        }

        public IWebElement iconoDeLimpiar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, ConfigurarUsuariosControllersConstantes.IconoDeLimpiar);
            }
        }

        public IWebElement seleccionarUnNumeroDePagina
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, ConfigurarUsuariosControllersConstantes.NumeroDePagina);
            }
        }

        public IWebElement clickEnElIconoDeCerrar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, ConfigurarUsuariosControllersConstantes.IconoDeCerrar);
            }
        }

        public IWebElement sortUsername
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.ClassName, ConfigurarUsuariosControllersConstantes.UsernameCss);
            }
        }

        public IWebElement sortEmail
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.CssSelector, ConfigurarUsuariosControllersConstantes.EmailCss);
            }
        }

        public IWebElement sortActive
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.CssSelector, ConfigurarUsuariosControllersConstantes.ActiveCss);
            }
        }

        public IWebElement clickOnAnUserToEditIt
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, ConfigurarUsuariosControllersConstantes.UserIdGrid);
            }
        }
    }
}