﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class AgregarUsuarioInformacionPersonalControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        public AgregarUsuarioInformacionPersonalControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement pestanaInfoPersonal
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.LinkText, InfoPersonalControllersConstantes.InfoPersonal); 
            }
        }

        public IWebElement campoNombre
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Nombre);
            }
        }

        public IWebElement campoApellido
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Apellido);
            }
        }

        public IWebElement campoEmail
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Email);
            }
        }

        public IWebElement campoDireccion
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Direccion);
            }
        }

        public IWebElement campoCiudad
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Ciudad);
            }
        }

        public IWebElement campoEstado
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Estado);
            }
        }

        public IWebElement campoCodigoPostal
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.CodigoPostal);
            }
        }

        public IWebElement campoPais
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Pais);
            }
        }

        public IWebElement campoTelefono
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Telefono); 
            }
        }

        public IWebElement campoCelular
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InfoPersonalControllersConstantes.Celular);
            }
        }
    }
}
