﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class AgregarUsuarioInformacionSistemaControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        public AgregarUsuarioInformacionSistemaControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
            AgregarUsuarioInfoSistemaControllersConstantes abcd = new AgregarUsuarioInfoSistemaControllersConstantes();
        }

        public IWebElement systemInfoTab
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, AgregarUsuarioInfoSistemaControllersConstantes.SystemInfoXpath);
            }
        }

        public IWebElement campoUsuario
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AgregarUsuarioInfoSistemaControllersConstantes.Usuario);
            }
        }

        public IWebElement campoContrasena
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AgregarUsuarioInfoSistemaControllersConstantes.Contrasena);
            }
        }

        public IWebElement campoConfirmarContrasena
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AgregarUsuarioInfoSistemaControllersConstantes.ConfirmarContrasena);
            }
        }

        public IWebElement campoIdioma
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AgregarUsuarioInfoSistemaControllersConstantes.Idioma);
            }
        }

        public IWebElement seleccionarAdministracionDeAgencia
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, AgregarUsuarioInfoSistemaControllersConstantes.AdministracionDeAgencia);
            }
        }

      
        public IWebElement seleccionarUsuarioActivoVerdadero
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, AgregarUsuarioInfoSistemaControllersConstantes.UsuarioActivoVerdadero);
            }
        }

        public IWebElement seleccionarUsuarioActivoFalso
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, AgregarUsuarioInfoSistemaControllersConstantes.UsuarioActivoFalso);
            }
        }
    }
}
