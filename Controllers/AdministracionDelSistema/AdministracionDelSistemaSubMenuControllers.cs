﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class AdministracionDelSistemaSubMenuControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        /*Constructor*/
        public AdministracionDelSistemaSubMenuControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement opcionConfigurarUsuarios
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, AdministracionDelSistemaControllersConstantes.ConfigurarUsuarios);
            }
        }
    }
}
