﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class InicioControllers
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private FuncionesDeSelenium funcionesDeSelenium;

        public InicioControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);

            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controller*/
        public IWebElement emisorCommercialTruck
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, InicioControllersConstantes.CommercialTruck);
                    
                    //_driver.FindElement(By.XPath("//td[@class='pointer programsTableTd']"));
            }
        }
    }
}
