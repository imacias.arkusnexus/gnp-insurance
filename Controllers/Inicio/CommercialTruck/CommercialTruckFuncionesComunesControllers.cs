﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class CommercialTruckFuncionesComunesControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        public CommercialTruckFuncionesComunesControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement botonDeAnterior
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.Anterior); 
            }
        }

        public IWebElement botonDeContinuar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.Continuar);
            }
        }

        public IWebElement botonDeGrabar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.ClassName, CommercialTruckCommonControllersConstantes.Guardar);
            }
        }

        public IWebElement botonDeCancelar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.Cancelar);
            }
        }

        public IWebElement botonDeCerrar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.Cerrar);
            }
        }

        public IWebElement botonDeContinuarDatosPoliza
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.ContinuarDatosPoliza);
            }
        }
    }
}
