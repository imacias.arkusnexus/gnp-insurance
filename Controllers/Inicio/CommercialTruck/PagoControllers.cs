﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class PagoControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        /*Constructor*/
        public PagoControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement seleccionarOpcionDePago
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, PagoControllersConstantes.Pago);
            }
        }

        public IWebElement pagarPoliza
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, CommercialTruckCommonControllersConstantes.Continuar);
            }
        }

        public IWebElement grabarPoliza
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.ClassName, CommercialTruckCommonControllersConstantes.Guardar);
            }
        }
    }
}
