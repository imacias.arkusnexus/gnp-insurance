﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class InformacionDelVehiculoControllers
    {
        private IWebDriver _driver;
        public FuncionesDeSelenium funcionesDeSelenium;

        public InformacionDelVehiculoControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers - Información del vehículo*/

        public IWebElement campoTipoVehiculo
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.TipoVehiculo);
            }
        }

        public IWebElement campoAnio
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Anio);
            }
        }

        public IWebElement campoMarca
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Marca);
            }
        }

        public IWebElement campoModelo
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Modelo);
            }
        }

        public IWebElement campoPaisVehiculo
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Pais);
            }
        }

        public IWebElement campoEstadoVehiculo
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Estado);
            }
        }

        public IWebElement campoVin
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Vin);
            }
        }

        public IWebElement campoPlacas
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Placas);
            }
        }

        public IWebElement campoPuertoEntrada
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.PuertoDeEntrada);
            }
        }

        public IWebElement campoCaja
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelVehiculoControllersConstantes.Caja);
            }
        }
    }
}
