﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class DatosDeLaPolizaControllers
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private FuncionesDeSelenium funcionesDeSelenium;

        /*Constructor*/
        public DatosDeLaPolizaControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);

            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement campoEstadoEntrada
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, DatosDeLaPolizaControllersConstantes.EstadoDeEntrada);
            }
        }

        public IWebElement campoCobertura
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, DatosDeLaPolizaControllersConstantes.Cobertura); 
            }
        }

        public IWebElement campoHora
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, DatosDeLaPolizaControllersConstantes.Hora);
            }
        }

        public IWebElement campoMinuto
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, DatosDeLaPolizaControllersConstantes.Minuto); 
            }
        }

        public IWebElement campoCalendario
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, DatosDeLaPolizaControllersConstantes.Calendario); 
            }
        }
    }
}
