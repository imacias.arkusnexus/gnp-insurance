﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class InformacionDelAseguradoControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        public InformacionDelAseguradoControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }
        
        /*Controllers*/
        public IWebElement campoNombre
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Nombre);
            }
        }

        public IWebElement campoRfc
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Rfc);
            }
        }

        public IWebElement campoArrendatario
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Arrendatario);
            }
        }

        public IWebElement campoTelefono
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Telefono); 
            }
        }

        public IWebElement campoPais
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Pais); 
            }
        }

        public IWebElement campoEstado
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Estado);
            }
        }

        public IWebElement campoCalle
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Calle);         
             }
        }

        public IWebElement campoColonia
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Colonia);
            }
        }

        public IWebElement campoCiudad
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Ciudad);
            }
        }

        public IWebElement campoNumero
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.Numero); 
            }
        }

        public IWebElement campoCodigoPostal
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.CodigoPostal); 
            }
        }

        public IWebElement campoCorreoElectronico
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, InformacionDelAseguradoControllersConstantes.CorreoElectronico);
            }
        }
    }
}
