﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class ConductoresAdicionalesControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium funcionesDeSelenium;

        public ConductoresAdicionalesControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement botonDeAgregar
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, ConductoresAdicionalesControllersConstantes.Agregar);
            }
        }
    }
}
