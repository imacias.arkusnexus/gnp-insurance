﻿using OpenQA.Selenium;
using Utilerias;
using Enum;
using ControllersConstantes;

namespace Controllers
{
    public class ComissionableSubMenuControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium _funcionesDeSelenium;
        
        public ComissionableSubMenuControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement clickOnSetupCommissionables
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, CommissionableSubMenuControllersConstants.SetupCommissionableId);
            }
        }

        public IWebElement clickOnAssignCommissionable
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, CommissionableSubMenuControllersConstants.AssignCommissionableId);
            }
        }
    }
}
