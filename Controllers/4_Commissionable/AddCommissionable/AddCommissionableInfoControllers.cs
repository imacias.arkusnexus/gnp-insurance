﻿using OpenQA.Selenium;
using Utilerias;
using ControllersConstantes;
using Enum;

namespace Controllers
{
    public class AddCommissionableInfoControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium _funcionesDeSelenium;

        public AddCommissionableInfoControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);    
        }

        public IWebElement commissionableInfoField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.CommissionableIdByName);
            }
        }

        public IWebElement commissionableNameField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.CommissionableNameByName);
            }
        }

        public IWebElement phoneField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.PhoneByName);
            }
        }

        public IWebElement homePageField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.HomePageByName);
            }
        }

        public IWebElement emailField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.EmailByName);
            }
        }

        public IWebElement noteField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.NoteByName);
            }
        }

        public IWebElement commissionableStatusField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, AddCommissionableInfoControllersContants.CommissionableStatusRadioButtonByXpath);
            }
        }

        public IWebElement addressField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.AddressByName);
            }
        }

        public IWebElement countryField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.CountryByName);
            }
        }

        public IWebElement stateField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.StateByName);
            }
        }

        public IWebElement cityField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.CityByName);
            }
        }

        public IWebElement zipField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.ZipByName);
            }
        }

        public IWebElement gmtField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, AddCommissionableInfoControllersContants.GmtByName);
            }
        }

        public IWebElement saveButton
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.XPath, AddCommissionableInfoControllersContants.SaveButtonByXpath);
            }
        }
    }
}
