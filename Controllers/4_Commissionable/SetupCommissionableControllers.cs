﻿using OpenQA.Selenium;
using Utilerias;
using Enum;
using ControllersConstantes;

namespace Controllers
{
    public class SetupCommissionableControllers
    {
        private IWebDriver _driver;
        private FuncionesDeSelenium _funcionesDeSelenium;

        public SetupCommissionableControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        public IWebElement addCommissionableLink
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, SetupCommissionablesControllersConstants.AddCommissionableLinkById);
            }
        }

        public IWebElement searchField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, SetupCommissionablesControllersConstants.SearchByName);
            }
        }

        public IWebElement searchValueField
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, SetupCommissionablesControllersConstants.SearchValueByName);
            }
        }

        public IWebElement searchButton
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.ClassName, SetupCommissionablesControllersConstants.SearchButtonByClass);
            }
        }

        public IWebElement clearButton
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, SetupCommissionablesControllersConstants.ClearButtonByName);
            }
        }
    }
}
