﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class LoginControllers
    {
        private IWebDriver _driver;
        private NgWebDriver ngDriver;

        private FuncionesDeSelenium funcionesDeSelenium;
        
        /*Constructor*/
        public LoginControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            ngDriver = new NgWebDriver(_driver);

            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement campoDeUsuario
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, LoginControllersConstantes.Usuario);
            }
        }

        public IWebElement campoDeContrasena
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, LoginControllersConstantes.Contrasena);
            }
        }

        public IWebElement botonDeLogin
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, LoginControllersConstantes.Login);
            }
        }
    }
}
