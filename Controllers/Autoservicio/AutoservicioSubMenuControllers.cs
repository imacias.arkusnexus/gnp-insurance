﻿using OpenQA.Selenium;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class AutoservicioSubMenuControllers
    {
        private IWebDriver _driver;

        private FuncionesDeSelenium funcionesDeSelenium;

        public AutoservicioSubMenuControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement opcionDeInfoPersonal
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, AutoservicioControllersConstantes.InfoPersonal);
            }
        }

        public IWebElement opcionDeCambiarClave
        {
            get
            {
                return funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Id, AutoservicioControllersConstantes.CambiarClave);
            }
        }
    }
}
