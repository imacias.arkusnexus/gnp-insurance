﻿using OpenQA.Selenium;
using Protractor;
using Enum;
using ControllersConstantes;
using Utilerias;

namespace Controllers
{
    public class CambiarClaveControllers
    {
        private IWebDriver _driver;
        private NgWebDriver _ngDriver;

        private FuncionesDeSelenium _funcionesDeSelenium;

        /*Constructor*/
        public CambiarClaveControllers(IWebDriver _driver)
        {
            this._driver = _driver;
            _ngDriver = new NgWebDriver(_driver);
            _funcionesDeSelenium = new FuncionesDeSelenium(_driver);
        }

        /*Controllers*/
        public IWebElement campoNuevaClave
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, CambiarClaveControllersConstantes.NuevaClaveName);
            }
        }

        public IWebElement campoConfirmarClave
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.Name, CambiarClaveControllersConstantes.ConfirmeClaveName);
            }
        }

        public IWebElement botonDeGrabar
        {
            get
            {
                return _funcionesDeSelenium.BuscarElemento(LocatorsEnum.Locators.LinkText, CambiarClaveControllersConstantes.botonDeGrabarLinkText);
            }
        }
    }
}
