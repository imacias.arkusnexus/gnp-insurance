﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace GNP
{
    public class InfoPersonalPage
    {
        private IWebDriver _driver;

        public InfoPersonalPage(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Controllers*/
        public IWebElement nombreField
        {
            get
            {
               return _driver.FindElement(By.Id("item_first_name"));
            }
        }

        private IWebElement apellidoField
        {
            get
            {
                return _driver.FindElement(By.Id("item_last_name")); ;
            }
        }
        
        private IWebElement correoField
        {
            get
            {
                return _driver.FindElement(By.Id("item_email"));
            }
        }
        
        private IWebElement direccionField
        {
            get
            {
                return _driver.FindElement(By.Id("item_address"));
            }
        } 

        private IWebElement idiomaField
        {
            get
            {
                return _driver.FindElement(By.Id("item_lang_id"));
            }
        }

        /*Methods*/
        public void LlenarFormaInfoPersona(string nombre, string apellido, string email, string direccion)
        {
            UtileriasDeEvidencias utilerias = new UtileriasDeEvidencias(_driver);

            try
            {
                utilerias.CambioDeFrame("menu_inbox");

                nombreField.Clear();
                nombreField.SendKeys(nombre);
                utilerias.ImprimirMensaje("Nombre: " + nombre);
              
                apellidoField.Clear();
                apellidoField.SendKeys(apellido);
                utilerias.ImprimirMensaje("Apellido: " + apellido);

                correoField.Clear();
                correoField.SendKeys(email);
                utilerias.ImprimirMensaje("Email: " + email);

                direccionField.Clear();
                direccionField.SendKeys(direccion);
                utilerias.ImprimirMensaje("Dirección: " + direccion);

                SelectElement select = new SelectElement(idiomaField);
                select.SelectByText("English");
                utilerias.CapturaDePantalla();
                utilerias.ImprimirMensaje("Idioma: " + select);

                utilerias.CambioDeFrame("menu_title"); //Close frame
            }

            catch (Exception e)
            {
                utilerias.ImprimirMensaje(e.Message);
            }
        }
    }
}
