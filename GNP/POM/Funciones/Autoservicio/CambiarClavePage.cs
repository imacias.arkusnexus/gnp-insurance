﻿using OpenQA.Selenium;

namespace GNP
{
    public class CambiarClavePage
    {
        private IWebDriver _driver;
             
        /*Constructor*/
        public CambiarClavePage(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Controllers*/
        private IWebElement nuevaClaveCampo {
            get
            {
                return _driver.FindElement(By.Id("item_usr_pw"));
            }  
        }
        
        private IWebElement confirmarNuevaClaveCampo
        {
            get
            {
                return _driver.FindElement(By.Id("item_usr_pw_confirm"));
            }
        }
        
        private IWebElement saveButton
        {
            get
            {
                return _driver.FindElement(By.Id("btnSave"));
            }
        }

        private IWebElement closeButton
        {
            get
            {
                return _driver.FindElement(By.Id("btnClose"));
            }
        }
      
        /// <summary>
        /// El usuario debe cambiar la clave 
        /// </summary>
        public void CambiarClaveModulo()
        {
            UtileriasDeEvidencias utileria = new UtileriasDeEvidencias(_driver);

            try
            {
                utileria.CambioDeFrame("menu_inbox");

                nuevaClaveCampo.SendKeys("agentqa");
                utileria.ImprimirMensaje("Nueva Clave: " + nuevaClaveCampo.ToString());

                confirmarNuevaClaveCampo.SendKeys("agentqa");
                utileria.ImprimirMensaje("Confirmar Clave: " + confirmarNuevaClaveCampo.Text.ToString());

                utileria.CapturaDePantalla();
                closeButton.Click();
                utileria.ImprimirMensaje("El usuario ha presionado el botón de 'Cerrar'.");
                
                utileria.CambioDeFrame("menu_title");
            }

            catch(NoSuchElementException ex)
            {
                utileria.ImprimirMensaje(ex.Message);
            }      
            
            catch(UnhandledAlertException ex)
            {
                utileria.ImprimirMensaje(ex.Message);
            }
        }
    }
}
