﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace GNP
{
    public class AgregarSiniestroDatosPoliza
    {
        private IWebDriver _driver;
        
        /*Constructor*/
        public AgregarSiniestroDatosPoliza(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Controllers*/
        private IWebElement numeroSiniestro
        {
            get
            {
                return _driver.FindElement(By.Id("txtSiniestro"));
            }
        }

        private IWebElement riesgo
        {
            get
            {
                return _driver.FindElement(By.Name("selectriesgo"));
            }
        }

        private IWebElement fechaDelSiniestro
        {
            get
            {
                return _driver.FindElement(By.Id("dtpFechasin_TextBox"));
            }
        }
        
        private IWebElement asegurado
        {
            get
            {
               return _driver.FindElement(By.Id("txtAsegurado"));
            }
        }

        private IWebElement certificado
        {
            get
            {
                return _driver.FindElement(By.Id("txtPoliza"));
            }
        }
        
        private IWebElement cerrarForma
        {
            get
            {
                return _driver.FindElement(By.Id("btnClose"));
            }
        }

        /*Methods*/
        public void AgregarUnSiniestroForma()
        {
            UtileriasDeEvidencias utilerias = new UtileriasDeEvidencias(_driver);

            utilerias.CambioDeFrame("menu_inbox");

            numeroSiniestro.SendKeys((AgregarSiniestrosContants.NumeroSiniestro).ToString());
            utilerias.ImprimirMensaje("Número de siniestro: " + numeroSiniestro.Text);

            SelectElement selectRiesgo = new SelectElement(riesgo);
            selectRiesgo.SelectByValue("ROBO");

            fechaDelSiniestro.Click();
            Thread.Sleep(3000);
            var calendarFechaSiniestro = _driver.FindElement(By.Id("ui-datepicker-div"));

            try
            {
                while (calendarFechaSiniestro.Displayed)
                {
                    utilerias.ImprimirMensaje("Calendario: Fecha de siniestros.");
                    utilerias.CapturaDePantalla();

                    IWebElement dayCalendar = _driver.FindElement(By.XPath("//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[2]"));
                    dayCalendar.Click();
                   
                    IWebElement monthCalendar = _driver.FindElement(By.ClassName("ui-datepicker-month"));
                    IWebElement yearCalendar = _driver.FindElement(By.ClassName("ui-datepicker-year"));
                    utilerias.ImprimirMensaje("Fecha: " + dayCalendar.Text + " / " + monthCalendar.Text + " / " + yearCalendar.Text);
                }
            }

            catch (Exception ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }
            
            asegurado.SendKeys(AgregarSiniestrosContants.Asegurado);
            utilerias.ImprimirMensaje("Asegurado: " + asegurado.Text);

            certificado.SendKeys((AgregarSiniestrosContants.Certificado).ToString());
            utilerias.ImprimirMensaje("Certificado: " + certificado.Text);

            utilerias.CapturaDePantalla();

            cerrarForma.Click();
            utilerias.ImprimirMensaje("Click en botón de cerrar forma.");

            utilerias.CambioDeFrame("menu_title"); //Close frame
        }
    }
}
