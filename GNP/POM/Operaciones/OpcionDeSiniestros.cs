﻿using OpenQA.Selenium;
using System.Threading;

namespace GNP
{
    public class OpcionDeSiniestros
    {
        private IWebDriver _driver;
                
        /*Constructor*/
        public OpcionDeSiniestros(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*Controllers*/
        private IWebElement agregarUnSiniestroLink
        {
            get
            {
                return _driver.FindElement(By.PartialLinkText("Agregar "));
            }
        } 

        private IWebElement listadoDeSiniestrosLink
        {
            get
            {
                return _driver.FindElement(By.LinkText("Listado De Siniestro"));
            }
        }

        private IWebElement administracionDeDocumentacionLink
        {
            get
            {
                return _driver.FindElement(By.PartialLinkText("Administración"));
            }
        }

        public enum OpcionOperacionesMenu
        {
            AgregarUnSiniestro,
            ListadoDeSiniestro,
            AdministracionDeDocumentacion,
            ListadoDeSiniestrosParaAgentes,
            PanelDeSiniestros,
            ConfiguracionDeNotificaciones,
            ReporteDeSiniestros,
            ReporteSumarizadoDeSiniestros
        }

        /*Methods*/
        public AgregarSiniestroDatosPoliza ClickEnLaOpcionDeAgregarUnSiniestro()
        {
            UtileriasDeEvidencias utilerias = new UtileriasDeEvidencias(_driver);

            agregarUnSiniestroLink.Click();
            Thread.Sleep(5000);
            utilerias.CapturaDePantalla();
            utilerias.ImprimirMensaje("Opción seleccionada: " + agregarUnSiniestroLink.Text);

            return new AgregarSiniestroDatosPoliza(_driver);
        }
    }
}
