﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace GNP
{
    public class MenuPom
    {
        IWebDriver driver;
        WebDriverWait wait;
        InfoPersonalPage infoPersonal;
        CloseFramePom closeLink;

        //  WebDriverUtileria utileria;

        /*Constructor*/
        public MenuPom(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            closeLink = new CloseFramePom(driver);
            infoPersonal = new InfoPersonalPage(driver);
            //  utileria = new WebDriverUtileria(_driver);
        }

       private void GoToFrame(string frameName)
        {
            driver.SwitchTo().DefaultContent().SwitchTo().Frame(driver.FindElement(By.Name(frameName)));
        }

        /*Methods*/
        public InfoPersonalPage menuOpciones()
        {
            try
            {
               GoToFrame("menu_main");
               Console.WriteLine("*** Cambio de frame: Menú ***");

                //_driver.SwitchTo().Frame(_driver.FindElement(By.Name("menu_main")));

                IWebElement funcionesOption = driver.FindElement(By.LinkText("Funciones"));
                funcionesOption.Click();

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                    .ElementToBeClickable(By.LinkText("Funciones")));

                Console.WriteLine("*** Option selected: " + funcionesOption.Text + " ***");
                Thread.Sleep(3000);

                
                IWebElement autoservicioOption = driver.FindElement(By.LinkText("Autoservicio"));
                autoservicioOption.Click();
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                    .ElementToBeClickable(By.LinkText("Autoservicio")));
                Thread.Sleep(3000);

                Console.WriteLine("*** Option selected: " + autoservicioOption.ToString() + " ***");

                IWebElement infoPersonaOption = driver.FindElement(By.LinkText("Info. Personal"));
                infoPersonaOption.Click();
                Thread.Sleep(3000);

                //Se invocó la clase CloseFrame();
                //infoPersonal.LlenarFormaInfoPersona(LoginConstants.Nombre, LoginConstants.Apellido, LoginConstants.Email, LoginConstants.Direccion);
                ////Nombre, Apellido, Email, Direccion);//
                closeLink.CloseFrame();  
            }

            catch (NoSuchElementException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (WebDriverTimeoutException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new InfoPersonalPage(driver);
        }
    }
}
