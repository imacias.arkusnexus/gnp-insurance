﻿using NUnit.Framework;
using TestCase;

namespace GNP
{
    public class TestCase01 : TestBase
    {
        UtileriasDeEvidencias utilerias;

        [Test, Description("Cambiar la contraseña.")]
        public void CambioContrasena()
        {
            LoginPage login = new LoginPage(driver);

            OpcionesDelMenu functionesOption = login.LoginWithValidCredentials(LoginConstants.username, LoginConstants.password);
            functionesOption.ClickEnAlgunaOpcionDelMenu(OpcionesDelMenu.OpcionesDelMenuEnum.Funciones);

            FuncionesMenuPom autoServicio = new FuncionesMenuPom(driver);
            autoServicio.ClickEnLaOpcionDeAutoservicio();

            CambiarClavePage cambiarClave = autoServicio.ClickEnCambiarClave();
            cambiarClave.CambiarClaveModulo();

            utilerias = new UtileriasDeEvidencias(driver);
            utilerias.ImprimirMensaje("Test Completed.");
        }

        [Test, Description("Llenar la forma de Info. Personal")]
        public void LlenarInfoPersonal()
        {
            LoginPage login = new LoginPage(driver);

            OpcionesDelMenu menuOption =  login.LoginWithValidCredentials(LoginConstants.username, LoginConstants.password);
            menuOption.ClickEnAlgunaOpcionDelMenu(OpcionesDelMenu.OpcionesDelMenuEnum.Funciones);

            FuncionesMenuPom autoServicio = new FuncionesMenuPom(driver);
            autoServicio.ClickEnLaOpcionDeAutoservicio();

            InfoPersonalPage infoPersonal = autoServicio.ClickEnLaOpcionDeInfoPersonal();
            infoPersonal.LlenarFormaInfoPersona(
                InfoPersonalLlenarFormaConstants.Nombre,
                InfoPersonalLlenarFormaConstants.Apellido,
                InfoPersonalLlenarFormaConstants.Email,
                InfoPersonalLlenarFormaConstants.Direccion
            );

            utilerias = new UtileriasDeEvidencias(driver);
            utilerias.ImprimirMensaje("Test Completed.");
        }

        [Test, Description("Verificar Administracion del sistema")]
        public void AdminSist()
        {
            LoginPage login = new LoginPage(driver);
            login.LoginWithValidCredentials(LoginConstants.username, LoginConstants.password);

            OpcionesDelMenu opcionesDelMenu = new OpcionesDelMenu(driver);
            opcionesDelMenu.ClickEnAlgunaOpcionDelMenu(OpcionesDelMenu.OpcionesDelMenuEnum.Administracion);

            AdministracionMenuPom administracion = new AdministracionMenuPom(driver);
            administracion.ClickEnAdministracionDelSistema();
        }

        [Test, Description("Agregar un siniestro.")]
        public void AddClaimPage()
        {
            LoginPage login = new LoginPage(driver);
            login.LoginWithValidCredentials("gnp", "p48256");

            OpcionesDelMenu funcionesMenu = new OpcionesDelMenu(driver);
            funcionesMenu.ClickEnAlgunaOpcionDelMenu(OpcionesDelMenu.OpcionesDelMenuEnum.Operaciones);

            OperacionesMenuPom operacionesOpciones = new OperacionesMenuPom(driver);
            operacionesOpciones.ClickEnUnaOpcionDeOperaciones(OperacionesMenuPom.OpcionOperacionesMenu.Siniestros);

            OpcionDeSiniestros siniestros = new OpcionDeSiniestros(driver);
            AgregarSiniestroDatosPoliza agregarSiniestro = siniestros.ClickEnLaOpcionDeAgregarUnSiniestro();
            agregarSiniestro.AgregarUnSiniestroForma();
            
            utilerias = new UtileriasDeEvidencias(driver);
            utilerias.ImprimirMensaje("Test Completed.");
        }
    }
}
 