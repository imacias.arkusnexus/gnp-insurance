﻿using GNP;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace TestCase
{
    public class TestBase
    {
        protected IWebDriver driver;
        protected const string url = "http://nexusinsuranceserver/gnpqa/";

        [SetUp]
        public void Setup()
        {
            CrossBrowser crossBrowser = new CrossBrowser();

            driver = crossBrowser.Browser(CrossBrowser.BrowserEnum.Chrome);
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Url = url;
            driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void CloseBrowser()
        {
            driver.Close();
            driver.Quit(); //Close all tabs.
            Console.WriteLine("** Cierra browser. **");
        }
    }
}
