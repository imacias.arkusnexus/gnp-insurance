﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace GNP
{
    public class CrossBrowser
    {
        public enum BrowserEnum { Chrome, Firefox }
        public IWebDriver Browser(BrowserEnum browser)
        {
            IWebDriver driver = null;
            try
            {
                switch (browser)
                {
                    case BrowserEnum.Chrome:
                        driver = new ChromeDriver();
                        break;

                    case BrowserEnum.Firefox:
                        driver = new FirefoxDriver();
                        break;

                    default:
                        Console.WriteLine("Browser selected is not available");
                        break;
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return driver;
        }
    }
}
