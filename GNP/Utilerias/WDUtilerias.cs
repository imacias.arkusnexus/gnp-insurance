﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace GNP
{
    public class WDUtilerias
    {
        private IWebDriver _driver;
        private WebDriverWait time;

        public WDUtilerias(IWebDriver _driver)
        {
            this._driver = _driver;
        }

        /*******************************/
        /******      METHODS     *******/
        /*******************************/

        /* Timer */
        public IWebElement GetElement(By by)
        {
            return time.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(by));
        }

        /* Find a element: findElement(By.X("xxxx") */
        public void SendKeys(By by, string texto)
        {
            GetElement(by).SendKeys(texto);
        }

        /* Click button */
        //public void Click(By by)
        //{

        //    GetElement(by).Click();
        //}

        //public IWebElement GetByText(By by, string text)
        //{
        //    return _driver.FindElements(by).FirstOrDefault(e => e.GetAttribute("InnerHtml").Contains(text));
        //}
    }
}
