﻿using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Threading;

namespace GNP
{
    public class UtileriasDeEvidencias 
    {
        private IWebDriver _driver;
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        /*Constructor*/
        public UtileriasDeEvidencias(IWebDriver _driver)
        {
            this._driver = _driver;
        }
        
        /*Time Stamp*/
        public String GetTimeStamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd_HH_mm_ss");
        }

        /*Screenshot*/
        public void CapturaDePantalla()
        {
            String timestamp = GetTimeStamp(DateTime.Now);
            Screenshot screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
            
            screenshot.SaveAsFile(@"C:\Users\Mind\Desktop\Log\Screenshot\" + timestamp+".png", ScreenshotImageFormat.Png);
            ImprimirMensaje("Screenshot successfully.");
        }

        /*Logs*/
        public void ImprimirMensaje(string logMessage)
        {
            Console.WriteLine("*** " + logMessage + " ***");
            logger.Info("*** " + logMessage + " ***");
            Thread.Sleep(5000);
         }

        /// <summary>
        /// Como el sitio está compuestos de Frame, éste método siempre se va estar utilizando para:
        ///     * Seleccionar una opción del menú.
        ///     * Trabajar con la pantalla principal.
        ///     * Cerrar el sitio.
        /// </summary>
        /// <param name="frame"></param>
        
        public void CambioDeFrame(string frame)
        {
            _driver.SwitchTo().DefaultContent().SwitchTo().Frame(_driver.FindElement(By.Name(frame)));

            switch (frame)
            {
                case "menu_inbox":
                    ImprimirMensaje("Cambio de frame: Principal.");
                    break;

                case "menu_main":
                    ImprimirMensaje("Cambio de frame: Menu.");
                    break;

                case "menu_title":
                    ImprimirMensaje("Cambio de frame: Title.");

                    IWebElement closeLink = _driver.FindElement(By.Id("btnSignOut"));
                    
                    closeLink.Click();

                    ImprimirMensaje("Usario ha cerrado sesión.");
                    break;

                default:
                    ImprimirMensaje("No hay opción seleccionada.");
                break;
            }
        }
    }
}
