﻿namespace GNP
{
    public static class InfoPersonalLlenarFormaConstants
    {
        public static string Nombre = "Diego";
        public const string Apellido = "Ventura";
        public const string Email = "test@mailinator.com";
        public const string Direccion = "Av. Pio Pico #123";
    }
}
