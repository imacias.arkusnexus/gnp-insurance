﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace GNP
{
    public class PantallaDeMensajeDeErrorPom
    {
        IWebDriver driver;
        WebDriverWait wait;
        UtileriasDeEvidencias utilerias;

        public PantallaDeMensajeDeErrorPom(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            utilerias = new UtileriasDeEvidencias(driver);
        }

        IWebElement errorMessageLabel => driver.FindElement(By.Id("lblError"));
        IWebElement backButton => driver.FindElement(By.Id("btnBack"));

        private void WaitTime(int seconds)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds + 1));
            DateTime dt = DateTime.Now.AddSeconds(seconds);
            wait.Until((driver) => {
                return DateTime.Now >= dt;
            });
        }

        public PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom VerificarMensajeDeError()
        {
            try
            {
                string errorMensaje = "Invalid username or password.";
                Assert.AreEqual(errorMensaje, errorMessageLabel.Text);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                    .ElementExists(By.Id("lblError")));

                utilerias.ImprimirMensaje("Error: " + errorMensaje);
                utilerias.ImprimirMensaje("Click en el botón de back.");

                backButton.Click();
                utilerias.ImprimirMensaje("Usuario has been clicked on \"Back\" button.");
                //   wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("backButton")));
            }

            catch(WebDriverTimeoutException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }

            return new PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom(driver);
        }
    }
}
