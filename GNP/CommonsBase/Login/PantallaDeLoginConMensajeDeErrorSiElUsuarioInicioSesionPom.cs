﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestCase;

namespace GNP
{
    public class PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom
    {
        IWebDriver driver;
        WebDriverWait wait;
        TestBase testCase;
        UtileriasDeEvidencias utilerias;

        public PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            utilerias = new UtileriasDeEvidencias(driver);
        }

        /*Controllers*/
        IWebElement usernameField => driver.FindElement(By.Id("usr_nm"));
        IWebElement passwordField => driver.FindElement(By.Id("usr_pw"));
        IWebElement errorMessageLabel => driver.FindElement(By.Id("lblError"));
        IWebElement backButton => driver.FindElement(By.Id("btnBack"));

        private void FillLogin(string username, string password)
        {
            try
            {
                usernameField.SendKeys(username);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("usr_nm")));
                utilerias.ImprimirMensaje("Username: " + username);

                passwordField.SendKeys(password);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("usr_pw")));
                utilerias.ImprimirMensaje("Password: " + password);

                IWebElement loginButton = driver.FindElement(By.XPath("/html/body/form/center/div/div/button"));
                loginButton.Click();

                utilerias.ImprimirMensaje("Se ha presionado el botón de Login.");
            }

            catch (NoSuchElementException ex)
            {
               utilerias.ImprimirMensaje(ex.Message);
            }

            catch (NullReferenceException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }
        }

        public void PantallaDeLoginConError(string escribirUsuario, string escribirContrasena)
        {
            //string url = "http://nexusinsurancserver/gnpqa/";
            //bool demo = Convert.ToBoolean(true);
           
            usernameField.Clear();
            FillLogin(escribirUsuario, escribirContrasena);

            try
            {
                //while (url != demo.ToString())
                //{
                    if (driver.FindElement(By.Id("divMessage")).Displayed)
                    {
                        string errorMessage = "El usuario ya está conectado / User already logged in.";

                        utilerias.ImprimirMensaje("Mensaje de error: " + errorMessage);
                        testCase.CloseBrowser();
                    }
              //  }

                //MenuPom auto = new MenuPom(_driver);
                //InfoPersonal info = auto.menuOpciones();
                //info.LlenarFormaInfoPersona(LoginConstants.Nombre, LoginConstants.Apellido, LoginConstants.Email, LoginConstants.Direccion);

                /*Agregar condicion si el usuario intenta logearse**/
                // IWebElement errorMessageDiv = _driver.FindElement(By.Id("divMessage"));
                //   string errorMessage = "El usuario ya está conectado / User already logged in.";

                //     Assert.AreEqual(errorMessageDiv.Text, errorMessage);
                //  wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                //    .TextToBePresentInElement(errorMessageDiv, errorMessage));

                //if (_driver.FindElement(By.Id("divMessage")).Displayed)
                //{
                //    string errorMessage = "El usuario ya está conectado / User already logged in.";

                //    Console.WriteLine("*** Mensaje de error: " + errorMessage + " ***");
                //    //testCase.CloseBrowser();
                //    //Console.WriteLine("** Cierra browser. **");
                //}

                //else
                //{

                //}

            }

            catch (NoSuchElementException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }

            catch (NullReferenceException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }

            catch (FormatException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }




           // return new MenuPom(_driver);
        }
    }
}
