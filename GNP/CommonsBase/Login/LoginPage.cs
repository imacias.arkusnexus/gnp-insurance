﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using TestCase;

namespace GNP
{
    public class LoginPage
    {
        IWebDriver driver;
        PantallaDeMensajeDeErrorPom errorPage;
        TestBase testCase;
        CloseFramePom closeFrame;
    //    PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom loginWithError;
        UtileriasDeEvidencias utileria;

        /*Constructor*/
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            errorPage = new PantallaDeMensajeDeErrorPom(driver);
            utileria = new UtileriasDeEvidencias(driver);
        }
       
        /*Controllers*/
        private IWebElement usernameFieldLogin
        {
            get
            {
                return driver.FindElement(By.Id("usr_nm"));
            }
        }

        private IWebElement passwordFieldLogin
        {
            get
            {
                return driver.FindElement(By.Id("usr_pw"));
            }
        }
        
        private IWebElement clickOnLoginButton
        {
            get
            {
                return driver.FindElement(By.XPath("/html/body/form/center/div/div/button"));
            }
        } 

        /*Method*/
        private void PantallaDeMensajeDeError()
        {
            if (driver.FindElement(By.Id("divMessage")).Displayed)
            {
                string errorMessage = "El usuario ya está conectado / User already logged in.";

                utileria.ImprimirMensaje(errorMessage);
                utileria.CapturaDePantalla();
            }
        }

        //private void LlenarCamposDeLogin(string escribeUsuario, string escribeContrasena)
        //{
        //    try
        //    {
        //        usernameFieldLogin.SendKeys(escribeUsuario);
        //       // wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("usr_nm")));
        //        utileria.ImprimirMensaje("Username: " + escribeUsuario); 

        //        passwordFieldLogin.SendKeys(escribeContrasena);
        //        //wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("usr_pw")));
        //        utileria.ImprimirMensaje("Password: " + escribeContrasena);
                
        //        clickOnLoginButton.Click();
        //        utileria.ImprimirMensaje("Se ha presionado el botón de login.");
        //    }

        //    catch (NoSuchElementException ex)
        //    {
        //        utileria.ImprimirMensaje(ex.Message);
        //    }

        //    catch (NullReferenceException ex)
        //    {
        //        utileria.ImprimirMensaje(ex.Message);
        //    }            
        //}

        public OpcionesDelMenu LoginWithValidCredentials(string escribeUsuario, string escribeContrasena)
        {
            try 
            {
                utileria.CapturaDePantalla();

                usernameFieldLogin.SendKeys(escribeUsuario);
                utileria.ImprimirMensaje("Username: " + escribeUsuario);

                passwordFieldLogin.SendKeys(escribeContrasena);
                utileria.ImprimirMensaje("Password: " + escribeContrasena);

                clickOnLoginButton.Click();
                utileria.ImprimirMensaje("Se ha presionado el botón de login.");
            }

            catch (InvalidOperationException ex)
            {
                utileria.ImprimirMensaje(ex.Message);
            }

            catch (NoSuchElementException ex)
            {
                utileria.ImprimirMensaje(ex.Message);
            }

            catch (NullReferenceException ex)
            {
                utileria.ImprimirMensaje(ex.Message);
            }

            return new OpcionesDelMenu(driver);
        }              

        //public void LoginWithValidCredentials(string username, string password)
        //{
        //    string url = "http://nexusinsurancserver/gnpqa/";
        //    string urlLogin = "http://nexusinsuranceserver/GnpQa/admin/menu_frame.aspx";
        //    bool urlBooleano = Convert.ToBoolean(true);
         
        //    usernameFieldLogin.Clear();
        // //   LlenarCamposDeLogin(username, password);

        //    try
        //    {
        //        while (url != urlBooleano.ToString() || urlLogin == urlBooleano.ToString())
        //        {
        //             loginWithError = new PantallaDeLoginConMensajeDeErrorSiElUsuarioInicioSesionPom(_driver);
        //            //  loginWithError.PantallaDeLoginConError("gnp", LoginConstants.password);

        //            /*Agregar condicion si el usuario intenta logearse**/
        //            if (_driver.FindElement(By.Id("divMessage")).Displayed)
        //            {
        //                string errorMessage = "El usuario ya está conectado / User already logged in.";

        //                utileria.ImprimirMensaje(" Mensaje de error:" + errorMessage);
        //                testCase.CloseBrowser();
        //            }

        //        }
        //    }

        //    catch (NoSuchElementException ex)
        //    {
        //        utileria.ImprimirMensaje(ex.Message);
        //    }

        //    catch (NullReferenceException ex)
        //    {
        //        utileria.ImprimirMensaje(ex.Message);
        //    }

        //    catch(FormatException ex)
        //    {
        //        utileria.ImprimirMensaje(ex.Message);
        //    }

        // //   return new OpcionesDelMenu(_driver);
        //}
    }
}
