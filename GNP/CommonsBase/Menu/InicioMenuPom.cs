﻿using OpenQA.Selenium;

namespace GNP
{
    public class InicioMenuPom
    {
        IWebDriver driver;

        /*Constructor*/
        public InicioMenuPom(IWebDriver driver)
        {
            this.driver = driver;
        }

        /**/
        private IWebElement servicioACliente => driver.FindElement(By.PartialLinkText("Servicio a Cliente"));
        
        public void HomePage()
        {
            driver.SwitchTo().DefaultContent().SwitchTo().Frame(driver.FindElement(By.Name("menu_inbox")));
        }
    }
}
