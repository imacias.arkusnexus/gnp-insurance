﻿using GNP;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNP
{
    public class AdministracionMenuPom
    {
        IWebDriver driver;
        UtileriasDeEvidencias utilerias;

        /*Constructor*/
        public AdministracionMenuPom(IWebDriver driver)
        {
            this.driver = driver;
            utilerias = new UtileriasDeEvidencias(driver);
        }

        /*Controllers*/
        IWebElement administracionDelSistema
        {
            get
            {
                return driver.FindElement(By.PartialLinkText("Sistema"));
            }
        }

       /*Methods*/
        public void ClickEnAdministracionDelSistema()
        {
            try
            {
                utilerias.CambioDeFrame("menu_main");

                administracionDelSistema.Click();
                utilerias.ImprimirMensaje("Option selected: " + administracionDelSistema.Text);
            }

            catch(Exception ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }
        }
    }
}
