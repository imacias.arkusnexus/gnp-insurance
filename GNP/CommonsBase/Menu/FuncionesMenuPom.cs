﻿using OpenQA.Selenium;

namespace GNP
{
    public class FuncionesMenuPom
    {
        IWebDriver driver;
        UtileriasDeEvidencias utilerias;

        /*Constructor*/
        public FuncionesMenuPom(IWebDriver driver)
        {
            this.driver = driver;
            utilerias = new UtileriasDeEvidencias(driver);
        }

        /*Controllers*/
        private IWebElement linkDeAutoservicio
        {
            get
            {
                return driver.FindElement(By.LinkText("Autoservicio"));
            }
        }

        /*Methods*/
        public void ClickEnLaOpcionDeAutoservicio()
        {
            linkDeAutoservicio.Click();
            utilerias.ImprimirMensaje("Opción Seleccionada: " + linkDeAutoservicio.Text);
            utilerias.CapturaDePantalla();
        }
        
        public InfoPersonalPage ClickEnLaOpcionDeInfoPersonal()
        {
            IWebElement linkDeInfoPersonal = driver.FindElement(By.LinkText("Info. Personal"));
            linkDeInfoPersonal.Click();

            utilerias.ImprimirMensaje("Opción Seleccionada: " + linkDeInfoPersonal.Text);
            utilerias.CapturaDePantalla();
            return new InfoPersonalPage(driver);
        }

        public CambiarClavePage ClickEnCambiarClave()
        {
            try
            {
                IWebElement linkDeCambiarClave = driver.FindElement(By.PartialLinkText("Clave"));
                linkDeCambiarClave.Click();

                utilerias.ImprimirMensaje("Opción Seleccionada: " + linkDeCambiarClave.Text);
            }

            catch (NoSuchElementException ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }
            return new CambiarClavePage(driver);
        }
    }
}
