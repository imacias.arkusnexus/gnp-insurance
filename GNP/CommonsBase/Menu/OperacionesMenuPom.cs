﻿using OpenQA.Selenium;

namespace GNP
{
    public class OperacionesMenuPom
    {
        IWebDriver driver;
        UtileriasDeEvidencias utilerias;

        public OperacionesMenuPom(IWebDriver driver)
        {
            this.driver = driver;
            utilerias = new UtileriasDeEvidencias(driver);
        }

        /*Controllers*/
        private IWebElement funcionesDeLaAgenciaLink
        {
            get
            {
                return driver.FindElement(By.PartialLinkText("Funciones")); 
            }
        }

        private IWebElement pagosLiquidacionesLink
        {
            get
            {
                return driver.FindElement(By.PartialLinkText("Liquidaciones"));
            }
        }

        private IWebElement reporteDeLaAgenciaLink
        {
            get
            {
                return driver.FindElement(By.PartialLinkText("Reportes"));
            }
        }

        private IWebElement siniestrosLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Siniestros"));
            }
        }

        private IWebElement clientesLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Clientes"));
            }
        }

        public enum OpcionOperacionesMenu
        {
            FuncionesDeLaAgencia,
            PagosLiquidaciones,
            ReportesDeLaAgencia,
            Siniestros,
            Clientes
        }

        /*Methods*/
        public void ClickEnUnaOpcionDeOperaciones(OpcionOperacionesMenu operacionesSubOption)
        {
            switch (operacionesSubOption)
            {
                case OpcionOperacionesMenu.FuncionesDeLaAgencia:
                    funcionesDeLaAgenciaLink.Click();
                    utilerias.CapturaDePantalla();
                    utilerias.ImprimirMensaje("Seleccionar opción de siniestros: " + funcionesDeLaAgenciaLink.Text);
               break;

                case OpcionOperacionesMenu.PagosLiquidaciones:
                    pagosLiquidacionesLink.Click();
                    utilerias.CapturaDePantalla();
                    utilerias.ImprimirMensaje("Seleccionar opción de siniestros: " + pagosLiquidacionesLink.Text);
                    break;

                case OpcionOperacionesMenu.ReportesDeLaAgencia:
                    reporteDeLaAgenciaLink.Click();
                    utilerias.CapturaDePantalla();
                    utilerias.ImprimirMensaje("Seleccionar opción de siniestros: " + reporteDeLaAgenciaLink.Text);
                    break;

                case OpcionOperacionesMenu.Siniestros:
                    siniestrosLink.Click();
                    utilerias.CapturaDePantalla();
                    utilerias.ImprimirMensaje("Seleccionar opción de siniestros: " + siniestrosLink.Text);
               break;

                case OpcionOperacionesMenu.Clientes:
                    clientesLink.Click();
                    utilerias.CapturaDePantalla();
                    utilerias.ImprimirMensaje("Seleccionar opción de siniestros: " + clientesLink.Text);
                break;
            }
        }
    }
}
