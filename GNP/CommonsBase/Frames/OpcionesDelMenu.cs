﻿//using Menu;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace GNP
{
    public class OpcionesDelMenu
    {
        IWebDriver driver;
        WebDriverWait wait;
        UtileriasDeEvidencias utilerias;

        /*Constructor*/
        public OpcionesDelMenu(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            utilerias = new UtileriasDeEvidencias(driver);
        }

        /*Controllers*/
        private IWebElement opcionDeInicioLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Inicio"));
            }
        }

        private IWebElement opcionDeFuncionesLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Funciones"));
            }
        }

        private IWebElement opcionDeAdministracionLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Administración"));
            }
        }

        private IWebElement opcionDeOperacionesLink
        {
            get
            {
                return driver.FindElement(By.LinkText("Operaciones"));
            }
        }

        public enum OpcionesDelMenuEnum
        {
            Inicio,
            Funciones,
            Autoservicio,
            Administracion,
            Operaciones
        }

        /*Métodos*/
        public void ClickEnAlgunaOpcionDelMenu(OpcionesDelMenuEnum opcionesMenu)
        {
            utilerias.CambioDeFrame("menu_main");
                     
            switch (opcionesMenu)
            {
                case OpcionesDelMenuEnum.Inicio:
                    opcionDeInicioLink.Click();

                    utilerias.ImprimirMensaje("Option selected: " + opcionDeInicioLink.Text);
                    utilerias.CapturaDePantalla();
               break;

                case OpcionesDelMenuEnum.Funciones:
                    opcionDeFuncionesLink.Click();

                    utilerias.ImprimirMensaje("Option selected: " + opcionDeFuncionesLink.Text);
                    utilerias.CapturaDePantalla();
                break;

                case OpcionesDelMenuEnum.Administracion:
                    opcionDeAdministracionLink.Click();

                    utilerias.ImprimirMensaje("Opción seleccionada: " + opcionDeAdministracionLink.Text);
                    utilerias.CapturaDePantalla();
                break;

                case OpcionesDelMenuEnum.Operaciones:
                    opcionDeOperacionesLink.Click();

                    utilerias.ImprimirMensaje("Option selected: " + opcionDeOperacionesLink.Text);
                    utilerias.CapturaDePantalla();
                break;

                default:
                    utilerias.ImprimirMensaje("No existe la opción seleccionada.");
                break;
            }
        }
    }
}
