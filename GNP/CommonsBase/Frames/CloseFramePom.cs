﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace GNP
{
    public class CloseFramePom
    {
        private IWebDriver driver;
       
        public CloseFramePom(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement clickEnElBotonDeCerrarSesion
        {
            get
            {
                return driver.FindElement(By.Id("btnSignOut")); 
            }
        }

        /// <summary>
        /// Esa función siempre se va ejecutar cuando se termine el test case. 
        /// </summary>

        public void CloseFrame()
        {
            UtileriasDeEvidencias utilerias = new UtileriasDeEvidencias(driver);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));

            try
            {
                utilerias.CambioDeFrame("menu_title");
            
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(clickEnElBotonDeCerrarSesion));
                clickEnElBotonDeCerrarSesion.Click();

                utilerias.ImprimirMensaje("Usario ha cerrado sesión.");
            }

            catch (Exception ex)
            {
                utilerias.ImprimirMensaje(ex.Message);
            }                        
        }
    }
}
