﻿namespace ControllersConstantes
{
    public class AgregarUsuarioInfoSistemaControllersConstantes
    {
        public const string SystemInfoXpath = "//a[@href = '#systemInfoTab']"; //"Info. sistema";
        public const string Usuario = "userName";
        public const string Contrasena = "password";
        public const string ConfirmarContrasena = "confirmPassword";
        public const string Idioma = "language";
        public const string AdministracionDeAgencia = "//div[@class='radio radio-inline radio-primary']";
     
        public const string UsuarioActivoVerdadero = "isActive_true";
        public const string UsuarioActivoFalso = "isActive_false";
    }
}
