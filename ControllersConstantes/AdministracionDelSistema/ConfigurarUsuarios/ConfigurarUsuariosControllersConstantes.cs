﻿namespace ControllersConstantes
{
    public class ConfigurarUsuariosControllersConstantes
    {
        public const string AgregarUsuario = "Agregar";
        public const string AgregarUsuarioId = "newUserLink";
        public const string Busqueda = "filter-field-name";
        public const string ValorDeBusqueda = "filter-field-value";
        public const string IconoDeBuscar = "filter-search";
        public const string IconoDeLimpiar = "filter-clear";
        public const string NumeroDePagina = "cmbHitsPage";
        public const string IconoDeCerrar = "btnBack";
       
        public const string UsernameCss = "#dtUsers>thead>tr>th:nth-child(1)";
        public const string EmailCss = "#dtUsers>thead>tr>th:nth-child(2)";
        public const string ActiveCss = "#dtUsers>thead>tr>th:nth-child(3)";

        public const string UserIdGrid = "UserNameLink_10436";
    }
}
