﻿namespace ControllersConstantes
{
    public static class InformacionDelVehiculoControllersConstantes
    {
        public const string TipoVehiculo = "vehicleType";
        public const string Anio = "year";
        public const string Marca = "make";
        public const string Modelo = "model";
        public const string Pais = "vehicleCountry";
        public const string Estado = "vehicleState";
        public const string Vin = "vin";
        public const string Placas = "plates";
        public const string PuertoDeEntrada = "entrancePort";
        public const string Caja = "box";
    }
}
