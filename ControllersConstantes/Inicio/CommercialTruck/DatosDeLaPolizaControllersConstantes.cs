﻿namespace ControllersConstantes
{
    public static class DatosDeLaPolizaControllersConstantes
    {
        public const string EstadoDeEntrada = "entranceState";
        public const string Cobertura = "package";
        public const string Hora = "startHour";
        public const string Minuto = "startMinutes";
        public const string Calendario = "startDate";
    }
}
