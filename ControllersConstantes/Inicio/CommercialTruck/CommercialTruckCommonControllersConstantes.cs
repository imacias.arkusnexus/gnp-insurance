﻿namespace ControllersConstantes
{
    public static class CommercialTruckCommonControllersConstantes
    {
        public const string Anterior = "//button[@class='btn btn-lg btn-prev']";
        public const string Continuar = "//button[@class='btn btn-lg  btn-next']";
        public const string Guardar = "ti-save";
        public const string Cancelar = "//span[@class='ti-trash text-danger']";
        public const string Cerrar = "//span[@class='ti-close pointer']";
        public const string ContinuarDatosPoliza = "//button[@class='btn btn-lg btn-next']";
    }
}