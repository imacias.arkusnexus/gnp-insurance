﻿namespace ControllersConstantes
{
    public static class InformacionDelAseguradoControllersConstantes
    {
        public const string Nombre = "name";
        public const string Rfc = "rfc";
        public const string Arrendatario = "lease";
        public const string Telefono = "phone";
        public const string Pais = "insuredCountry";
        public const string Estado = "insuredState";
        public const string Calle = "street";
        public const string Colonia = "colony";
        public const string Ciudad = "city";
        public const string Numero = "exteriorNumber";
        public const string CodigoPostal = "zipCode";
        public const string CorreoElectronico = "email";

    }
}
