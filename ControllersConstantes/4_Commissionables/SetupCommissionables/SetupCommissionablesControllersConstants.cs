﻿namespace ControllersConstantes
{
    public static class SetupCommissionablesControllersConstants
    {
        public const string AddCommissionableLinkById = "newPartnerLink";
        public const string SearchByName = "filter-field-name";
        public const string SearchValueByName = "filter-field-value";
        public const string SearchButtonByClass = "ei-search";
        public const string ClearButtonByName = "filter-clear";
        public const string PagesById = "cmbHitsPage";

        public const string CommissionableIdByCss = "#dtPartners>thead>tr>th.sorting_asc";
        public const string CommissionableNameByCss = "#dtPartners>thead>tr>th:nth-child(2)";
        public const string EmailByCss = "#dtPartners>thead>tr>th:nth-child(3)";
        public const string PhoneByCss = "#dtPartners>thead>tr>th:nth-child(4)";
        public const string CityByCss = "#dtPartners>thead>tr>th:nth-child(5)";
    }
}
