﻿namespace ControllersConstantes
{
    public static class ContactInfoControllersConstants
    {
        public const string FirstNameByName = "firstName";
        public const string LastNameByName = "latName";
        public const string EmailByName = "userEmail";
        public const string UsernameByName = "username";
        public const string PasswordByName = "password";
        public const string ConfirmPasswordByName = "confirmPassword";
        public const string LanguageByName = "language";
    }
}
