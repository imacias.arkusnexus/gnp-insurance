﻿namespace ControllersConstantes
{
    public static class AddCommissionableInfoControllersContants
    {
        public const string CommissionableIdByName = "no";
        public const string CommissionableNameByName = "name";
        public const string PhoneByName = "phone";
        public const string HomePageByName = "webSiteUrl";
        public const string EmailByName = "email";
        public const string NoteByName = "comments";
        public const string CommissionableStatusRadioButtonByXpath = "";

        public const string AddressByName = "address";
        public const string CountryByName = "country";
        public const string StateByName = "state";
        public const string CityByName = "city";
        public const string ZipByName = "zip";
        public const string GmtByName = "timeZoneId";
        public const string SaveButtonByXpath = "//button[@class='btn btn-lg btn-next']";
    }
}
