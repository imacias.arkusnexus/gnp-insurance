﻿namespace ControllersConstantes
{
    public static class InfoPersonalControllersConstantes
    {
        public const string InfoPersonal = "Info. personal";
        public const string Nombre = "firstName";
        public const string Apellido = "lastname";
        public const string Email = "email";
        public const string Direccion = "address";
        public const string Ciudad = "city";
        public const string Estado = "state";
        public const string CodigoPostal = "zip";
        public const string Pais = "country";
        public const string Telefono = "phone";
        public const string Celular = "cell";
        public const string Idioma = "language";
        public const string GrabarTextLink = "Grabar";
        public const string Cerrar = "btnBack";

        public const string GrabarXpath = "//button[@class='btn btn-lg btn-next']";
    }
}
