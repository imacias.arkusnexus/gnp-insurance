﻿namespace ControllersConstantes
{
    public static class OpcionesDelMenuControllersConstantes
    {
        public const string Autoservicio = "item6";
        public const string AdministracionDelSistema = "item14";
        public const string Comisionable = "item125";
        public const string FuncionesDeLaAgencia = "item20";
        public const string PagosLiquidaciones = "item107";
        public const string ReportesDeLaAgencia = "item21";
    }
}
